#!/usr/bin/python3
# -*- coding: utf-8 -*-
#ok
import random
import sys

#from Arma import * # <- usar a referencia
from Item import *
class Arma(Item):
	def __init__(self,nome,impacto,rapidez):
		super().__init__(nome)
		self.impacto = impacto
		self.rapidez = rapidez

	def getNome(self):
		return super().getNome()

	def getAtaque(self):
		return (self.impacto * self.rapidez)

	def getSintese(self):
		return "\n#########################################################\n"+"\nItem de Ataque, Nome do item:"+self.getNome()+"\nCapacidade de ataque efetivo:"+str(self.getAtaque())+"\nCapacidade de defesa:0, Capacidade de Cura:0 \n"+"#########################################################\n"		