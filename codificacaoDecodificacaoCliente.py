# -*- coding: utf-8 -*-
import random
import binascii
import string
import difflib

#Esta funcao conta quantos caracteres tera no tamanho da mensagem.
#retorna um numero entre 1 ~ 4, sendo 4 pela limitação de 1024 
#caracteres

def rectameretornadecimal(tamanho):
	if(tamanho > 1 and tamanho < 10):
		return 1
	if(tamanho > 10 and tamanho < 100):
		return 2
	if(tamanho > 100 and tamanho < 1000):
		return 3
	if(tamanho > 1000):
		return 4

def escolhadainstrucaoaserenviada(escolhaistrucao):
	if(escolhaistrucao == 1): # Pegar um item da ilha
		escolha = 'CATI'
		return escolha
	if(escolhaistrucao == 2): # Coletar uma magia da ilha
		escolha = 'CATM'
		return escolha
	if(escolhaistrucao == 3): # Lista todas as direcoes possiveis a partir da ilha
		escolha = 'DIRC'
		return escolha
	if(escolhaistrucao == 4): # Pede o encerramento da conexao
		escolha = 'EXIN'
		return escolha
	if(escolhaistrucao == 5): # Pede um relatorio dos personagens da ilha
		escolha = 'GETI'
		return escolha
	if(escolhaistrucao == 6): # Pede um relatorio atual da situação atual do jogador
		escolha = 'GETS'
		return escolha
	if(escolhaistrucao == 7): # Pede uma lista de todos os itens do jogador
		escolha = 'LSTC'
		return escolha
	if(escolhaistrucao == 8): # Pede uma lista completa de todos os itens presentes na ilha
		escolha = 'LSTI'
		return escolha
	if(escolhaistrucao == 9): # Pede a informação se existe alguma magia presente na ilha atual do jogador
		escolha = 'LSTM'
		return escolha
	else:				# Erro o que foi pedido não foi encontrado, instrucao sera ignorada
		escolha = 'ERRO'
		return escolha	



# Funcao que constroi a mensagem de Ataque , no lado cliente
# argumento um numero hexa playerID
# argumento um numero decimal inimigoposicao
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaATQP(inimigoposicao,playerID):
	codigoDainstrucao = 'ATQP' # codigo
	tamtotal_da_mensagem = 0   #0~1024
	strininimigo = str(inimigoposicao)
	mensagemConst = codigoDainstrucao + ' '+' ' +'\r\n' + playerID +' '+ '\r\n'+strininimigo+'\r\n'
	tammensagemparcial = len(mensagemConst)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espacao em branco
	#finalcode = codigoDainstrucao + ' '+ str(tamtotal_da_mensagem) +' '+'\r\n' + Idplayer.decode("latin_1") +' '+'\r\n'+strininimigo+'\r\n'
	finalcode = codigoDainstrucao + ' '+ str(tamtotal_da_mensagem) +' '+'\r\n' + playerID +' '+'\r\n'+strininimigo+'\r\n'
	return finalcode

# Funcao que constroi a mensagem de begin conecao , no lado cliente
# argumento uma string entre 5~32 caracteres
# argumento um numero decimal entre 1~3
# um endereço ip
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaBEGG(strNamePlayer,numTipoDoJogador,enderecoip):
	codigoDainstrucao = 'BEGG' # codigo
	tamtotal_da_mensagem = 0   #0~1024
	mensagemparcial = codigoDainstrucao+' '+' '+'\r\n'+strNamePlayer+' '+'\r\n'+str(numTipoDoJogador)+' ' +'\r\n'+str(enderecoip)+'\r\n' 		
	tammensagemparcial = len(mensagemparcial)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espaco em branco
	finalcode = codigoDainstrucao+' '+str(tamtotal_da_mensagem)+' '+'\r\n'+strNamePlayer+' '+'\r\n'+str(numTipoDoJogador)+' ' +'\r\n'+str(enderecoip)+'\r\n' 		
	return finalcode


# Funcao que constroi a mensagem JOGI, mensagem que tem a capacidade de encapsular muitas mensagens
# argumento escolhaistrucao que o jogador deseja realizar str
# argumento um numero hexa playerID
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaJOGI(escolhaistrucao,playerID):
	codigoDainstrucao = 'JOGI'
	strescolha =  escolhadainstrucaoaserenviada(escolhaistrucao)
	mensagemConst = codigoDainstrucao + ' '+' ' +'\r\n' +strescolha+' '+ '\r\n'+playerID+' '+'\r\n'
	tammensagemparcial = len(mensagemConst)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espaco em branco
	finalcode = codigoDainstrucao + ' '+str(tamtotal_da_mensagem)+' ' +'\r\n' +strescolha+' '+ '\r\n'+playerID+' '+'\r\n'
	return finalcode

# Funcao que constroi a mensagem MOVP, mensagem que comunica o comando do usuário de mudar de ilha
# argumento um numero em hexa playerID
# argumento a identificacao da saida da ilha do usuário
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaMOVP(playerID,caminhoIlha):
	codigoDainstrucao = 'MOVP'
	mensagemConst 	  = codigoDainstrucao + ' '+' ' +'\r\n' +playerID+' '+ '\r\n'+str(caminhoIlha)+' '+'\r\n'
	tammensagemparcial = len(mensagemConst)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espaco em branco
	finalcode = codigoDainstrucao + ' '+str(tamtotal_da_mensagem)+' ' +'\r\n' +playerID+' '+ '\r\n'+str(caminhoIlha)+' '+'\r\n'
	return finalcode


# Funcao que constroi a mensagem USEI, mensagem que comunica o comando do usuário de utilizar um item do seu saco de itens
# argumento um numero em hexa do playerID
# argumento o codigo que identifica o item dentro do saco de itens
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaUSEI(playerID,codigoItem):
	codigoDainstrucao = 'USEI'
	mensagemConst 	  = codigoDainstrucao + ' '+' ' +'\r\n' +playerID+' '+ '\r\n'+str(codigoItem)+' '+'\r\n'
	tammensagemparcial = len(mensagemConst)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espaco em branco
	finalcode = codigoDainstrucao + ' '+str(tamtotal_da_mensagem)+' ' +'\r\n' +playerID+' '+ '\r\n'+str(codigoItem)+' '+'\r\n'
	return finalcode

# Funcao do lado cliente que decodifica todas as resposta do servidor
# argumento : resposta do servidor
# Retorna uma mensagem pronta para o cliente
def decodificaResposta(mensagem):
	prefixo = mensagem[0:4]
	if(prefixo=='0000'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		resposta = [0,mensagemparcial[1],"Conexão aceita, aguardando inicio de jogo"]
		# apresenta codigo instrucao, Id do 
		return  resposta # retorna um codigo que representa a resposta , a identificacao do jogador, mensagem
	if(prefixo=='1111'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		resposta = [1,mensagemparcial[1],"Jogo iniciou, aguardando comandos !!!"]
		return  resposta # retorna um codigo que representa a resposta , a identificacao do jogador, mensagem
	if(prefixo=='7777'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		resposta = [7,mensagemparcial[1],"Jogo ja iniciou, aguardando comandos !!!"]
		return  resposta # retorna um codigo que representa a resposta , a identificacao do jogador, mensagem
	if(prefixo=='2222'): # primeiros 4 caracteres da mensagem
		#divide por espaco em branco
		div = (mensagem.split())
		tammensagem = int(div[1]) # tamanho da mensagem
		mensagemparcial = mensagem.split('\r\n') # divide a mensagem em partes segundo a numeracao linhas
		if(tammensagem==len(mensagem)): #ok não foi distorcido 
			resposta = [2,int(mensagemparcial[1]),int(mensagemparcial[3]),int(mensagemparcial[2]),mensagemparcial[4]] # retorna codigo instrucao,se é segmentado,o quanto ele precisa ler, id do pacote e mensagem
			return resposta
		else:
			return None
	if(prefixo=='5555'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		message = len(mensagemparcial)
		if(int(mensagemparcial[1]) == len(mensagem)): # ok
			respotamensagem = mensagem[10:(len(mensagem)-2)]
			resposta = [5,respotamensagem] # retorna o codigo da resposta e a mensagem
			return resposta
		else :	 # mensagem corrompida
			return None
	if(prefixo=='9999'): # primeiros 4 caracteres da mensagem
		respotamensagem = mensagem[6:(len(mensagem)-2)]
		resposta = [9,respotamensagem]
		return resposta
	else:
		return None
		
class JuntaMensagem(object):
	def __init__(self,tam,numpacote,mensagem):
		self.tam = tam
		self.numpacote = int(numpacote)
		self.mensagem = mensagem
	def getTam(self):
		return self.tam
	def getnumPacote(self):
		return self.numpacote
	def getMensagem(self):
		return self.mensagem


def genRandomwords(numb):
	k = '' # vazio
	for i in range(numb):
		k += random.SystemRandom().choice(string.ascii_uppercase +' '+string.digits)
	return k

# recebe um conjunto de mensagens de reposta e 
# carrega o tam do pacote, o numero e a mensagem
# para a posterior uniao
class JuntaMensagem(object):
	def __init__(self,tam,numpacote,mensagem):
		self.tam = tam
		self.numpacote = int(numpacote)
		self.mensagem = mensagem
	def getTam(self):
		return self.tam
	def getnumPacote(self):
		return self.numpacote
	def getMensagem(self):
		return self.mensagem


# recebe um vetor de respostas
def Unirespostas(vetor):
	if (len(vetor) == 1):
		j = decodificaResposta(vetor)
		exit()
		return (j[4])

	else:
		copy = []
		stream = ''
		i=0
		for i in range(len(k1)):
			copy.append(decodificaResposta(k1[i]))
			catcher =  copy[i]
			stream += catcher[4]	
		return stream




# exit()

# mensagem = genRandomwords(50*10) # gera palavras aleatorias
# k1 = respMensagem(mensagem) # devolve um vetor contendo todos os pacotes a enviar
# print(k1[0])
# exit()
# x = Unirespostas(k1)
# if(x==mensagem):
# 	print("Perfeito")

# #print(len(k1))
# exit()	
# print(Unirespostas(k1))

# if(mensagem == x):
# 	print("Nice")


# print('A mensagem:\n')
# print(mensagem)
# k1 = respMensagem(mensagem) # devolve um vetor contendo todos os pacotes a enviar
# if (k1 == 1):
# 	k = decodificaResposta(k1)
# else:
# 	copy = []
# 	stream = ''
# 	for i in range(len(k1)):
# 		copy.append(decodificaResposta(k1[i]))
# 		catcher =  copy[i]
# 		stream += catcher[4]

# if(mensagem == stream):
# 	print("Nice")
# # k = len(j)
# # i=0
# # a = []
# # total = ''
# # if(type(j[0]) == list):
# # 	for i in range(k):
# # 		copy = decodificaResposta(j[i])
# # 		if(copy == None):
# # 			print('problem')
# # 			exit()
# # 		l = JuntaMensagem(copy[2],copy[3],copy[4])
# # 		a.append(l)
# # 		if(a[i].getnumPacote()==i):
# # 			total+=a[i].getMensagem()

