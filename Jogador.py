#!/usr/bin/python3
# -*- coding: utf-8 -*-
#ok
import random
import sys

	
#-------------------------------------------------------------------------------------------------------------------------#			
class Jogador(object):
		# construtor na classe jogador
	def __init__(self,posicaoArray,posicaoArrayPersonagens,pescador):
		self.posicaoarray =posicaoArray  # composto por dois numeros uma posicao no array de ilhas e a posicao no vetor de personagensAtivos
		self.posicaoArrayPersonagem = posicaoArrayPersonagens
		#self.identification = format(random.randrange(4294967296),'02x') # codigo único do jogador()
		self.identification = hex(random.randrange(255)) # codigo entre 0 e 256
		self.pescador = pescador
	
		# seta novas posicoes de ilha e posicao de personagem
	def setnovaposicao(self,novaPosicaoArrayilhas,novaPosicaoArrayPersonagens): # seta nova posicao para o jogador
		self.posicaoarray = novaPosicaoArrayilhas
		self.posicaoArrayPersonagem  = novaPosicaoArrayPersonagens
		
		# retorna a referencia do pescador
	def getPescador(self):
		return self.pescador
	
		# retorna posicao no vetor
	def getPosicao(self): # retorna a posicao do jogador (ilha, numero dele no vetor)
		vetorrespota = [self.posicaoarray,self.posicaoArrayPersonagem]
		return vetorrespota
		# retorna a id do jogador
	def getId(self):
		return (self.identification)
		# gera outro caso seja achado com o mesmo numero de id
	def genOther(self):
		self.identification = hex(random.randrange(255)) # codigo único do jogador()
		return self.getId()
