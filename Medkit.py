#!/usr/bin/python3
# -*- coding: utf-8 -*-
#ok
import random
import sys

# from Medkit import * # <- referencia
from Item import *
class Medkit(Item):
	def __init__(self,nome,cura):
		super().__init__(nome)
		self.cura = cura

	def getNome(self):
		return super().getNome()

	def getCura(self):
		return self.cura

	def getSintese(self):
		return "\n#########################################################\n"+"\nItem de Medical Kit, Nome do item:"+self.getNome()+"\nCapacidade de cura efetivo:"+str(self.getCura())+"\nCapacidade de Ataque:0, Capacidade de defesa:0\n"+"#########################################################\n"	

