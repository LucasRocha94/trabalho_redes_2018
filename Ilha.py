#!/usr/bin/python3
# -*- coding: utf-8 -*-
#ok
import random
import sys
from Pescador import *
from Personagem import *
from Magia import *
from Item import *
from Medkit import *
from Arma import *
from Capacidadededefesa import *



class Ilha(object):
	
	def __init__(self,nome):
		self.nomeIlha = nome
		self.conchaMagica = None    # contem 1 magia
		self.vetorDirecao = [] 	    # contem varios objetos tipo ilha
		self.personagensAtivos = [] # podendo conter varios personagens
		self.bauItem = None			# bau de itens contem 1 item

	def addPersonagem(self,personagem): # adiciona personagem na ilha 
		self.personagensAtivos.append(personagem)
	
	# verifica se esta morto e deleta do jogo
	def verificaMorto(self):
		tam = len(self.personagensAtivos) # devolve tamanho de vetor com personagens
		i = 0
		for i in range(tam):
			if(self.personagensAtivos[i].getVida()==0): # personagem esta morto
				del self.personagensAtivos[i] #remove o personagem morto
	
	def getNumeroPersonagens(self):
		return (len(self.personagensAtivos))

	#Ainda precisa ser testado
	def getPersonagem(self,num): # trocar entre ilhas
		backup = self.personagensAtivos[num]
		del self.personagensAtivos[num]
		return  backup

	
	def getNome(self):
		return self.nomeIlha

	def addMagia(self, magia):
		self.conchaMagica = magia
		# retorna 1 caso haja magia

	def verificaSeHaMagia(self):
		if(self.conchaMagica != None):
			return 1
		else:
			return 0
	
		# passa o parametro num que eh o numero do 
		# retorna 1 caso obtenha sucesso na transferencia da magia
	def getMagia(self, num):
		if(num > len(self.personagensAtivos)):
			return 0
		else:
			if(type(self.personagensAtivos[num]) is Pescador):
				self.personagensAtivos[num].addMagias(self.conchaMagica)
				self.conchaMagica = None
				return 1
			else:
				return 0

	def printpersonagens(self):
		tam = len(self.personagensAtivos) # devolve tamanho de vetor com personagens
		i=0
		message = ""
		if(tam > 0):
			for i in range(tam):
				
			#	if(type(self.personagensAtivos[i]) is Pescador): # verifica se eh pescador para chamar metodo do pescador
			#		message += "\n###############################\n"+"Numero do personagem: "+str(i)+"\n"+self.personagensAtivos[i].getStatusPescador()
					
			#	else:	# verifica se eh personagem e chama metodo adequado
				message += "\n###############################\n"+"Numero do personagem:"+str(i)+"\n"+self.personagensAtivos[i].getStatus()
					
			return message
		else:
			return "Não há personagens Ativos"

	def addIlha(self, ilha):
		if(len(self.vetorDirecao) == 0): # caso o valor do tamanho do vetor seja zero ele adiciona direto
			self.vetorDirecao.append(ilha)
			return None
		else:
			try: # tenta localizar dentro do vetor a ilha que foi anteriomente inserida
				index = self.vetorDirecao.index(ilha)
			except ValueError:
				self.vetorDirecao.append(ilha)
				return None
			else:
				return None

						
	
	def getListaIlhas(self):
		tam = len(self.vetorDirecao)
		if(tam == 0):
			return "Sem direcoes a partir da ilha"
		i = 0
		resposta = ""
		for i in range(tam):
			resposta += "\n#########################################################\n"+"Nome da Ilha:"+self.vetorDirecao[i].getNome()+"\n"+"Direcao no mapa:"+str(i)+"\n"+"#########################################################\n"	

		return resposta
	def getdirecao(self,num):
		return self.vetorDirecao[num]

	def batalhaPersonagens(self,num1,num2): #realizar batalha
		defesa1 = self.personagensAtivos[num1].getDefesa() + 	self.personagensAtivos[num1].getVida() 
		defesa2 = self.personagensAtivos[num2].getDefesa() + 	self.personagensAtivos[num2].getVida() 
		ataque1 = self.personagensAtivos[num1].getAtaque()
		ataque2 = self.personagensAtivos[num2].getAtaque()
		var1 = (ataque1 - defesa2)
		var2 = (ataque2 - defesa1) # ataque[num2] - defesa
		if(var1 > var2): # num1 ganha de num2
			if (type(self.personagensAtivos[num1]) is Pescador): # se eh do tipo pescador o dano deve ser repassado para a armadura
				self.personagensAtivos[num1].repassandoDanoPescador(ataque2) # repassado o ataque do que morreu
				if(type(self.personagensAtivos[num2]) is Pescador): # Se o personagem 2 eh pescador repassa magias
					magiasdomorto = self.personagensAtivos[num2].getAllmagias() # pega as magias do personagem que morreu
				#	ponteiro = self.personagensAtivos[num1]
					self.personagensAtivos[num1].addmagiasmorto(magiasdomorto)
					del self.personagensAtivos[num2] # deleta o perdedor
					message = "Personagem 2 morreu"
				#	indice = self.personagensAtivos.index(ponteiro) 
					vetor = [2,message]
					return vetor
				else:
					del self.personagensAtivos[num2] # deleta o perdedor
					message = "Personagem 2 morreu"
				#	indice = self.personagensAtivos.index(ponteiro) 
					vetor = [2,message]
					return vetor

			else:
				self.personagensAtivos[num1].repassandoDano(ataque2)
				#ponteiro = self.personagensAtivos[num1]
				del self.personagensAtivos[num2] # deleta o perdedor
			#	indice = self.personagensAtivos.index(ponteiro)
				message = "Personagem 2 morreu"
				vetor = [2,message]
				return vetor
			
		if(var1 < var2): # num2 ganha de num1
			if (type(self.personagensAtivos[num2]) is Pescador): # se eh do tipo pescador o dano deve ser repassado para a armadura
				if(type(self.personagensAtivos[num1]) is Pescador): # caso seja do tipo pescador transfere magias 
					self.personagensAtivos[num2].repassandoDanoPescador(ataque1) # eh repassado o ataque do 2
					magiasdomorto = self.personagensAtivos[num1].getAllmagias()
					self.personagensAtivos[num2].addmagiasmorto(magiasdomorto)
				#	ponteiro = self.personagensAtivos[num2]
					del self.personagensAtivos[num1] # deleta o perdedor
				#	indice = self.personagensAtivos.index(ponteiro)
					message = "Jogador 1 Morreu"
					vetor = [1,message]
					return vetor
				else:
					del self.personagensAtivos[num1] # deleta o perdedor
				#	indice = self.personagensAtivos.index(ponteiro)
					message = "Jogador 1 Morreu"
					vetor = [1,message]
					return vetor

			else:
			#	ponteiro = self.personagensAtivos[num2]
				self.personagensAtivos[num2].repassandoDano(ataque1) # eh repassado o ataque do 1
				del self.personagensAtivos[num1] # deleta o perdedor
			#	indice = self.personagensAtivos.index(ponteiro)
				message = "Jogador 1 Morreu"
				vetor = [1,message]
				return vetor
			
		if(var1 == var2): # apaga ambos em caso de empate
			obj = self.personagensAtivos[num1] # salva antes da referencia mudar
			del self.personagensAtivos[num2]		
			numb = self.personagensAtivos.index(obj) 
			del self.personagensAtivos[numb]
			message = "Ambos os jogadores morreram"
			vetor = [3,message]
			return vetor
	 
	
	def addItem(self,itemNovo): # adiciona item no saco de itens
		self.bauItem = itemNovo
	
	# Numero do pescador eh passado como argumento
	def getItemParaPescador(self,numpescador):
		if(self.itemExist()!=None):
			if(self.getNumeroPersonagens() < numpescador): # verifica numero de personagens ativos
				return None #erro numero de persconagens eh insuficiente parametro errado
			else:
				if(type(self.personagensAtivos[numpescador]) is Pescador):
					self.personagensAtivos[numpescador].addItens(self.getItem()) # entrega item para pescador selecionado
					return 1
					
				else:# nao tem saco de itens pois nao eh pescador
					return None



	def getItem(self):
		if(self.bauItem==None):
			return None
		else:
			backup = self.bauItem
			self.bauItem = None
			return backup

	def itemExist(self):
		if(self.bauItem == None):
			return None

		else:
			return 1

	def quantidadeIlhasapontadas(self): # retorna quantidade de ponteiros dentro do vetor, idealmente deve ser 2
		if(self.vetorDirecao==None):
			return 0
		else:
			return len(self.vetorDirecao)

	def retornalistaitensnaIlha(self):
		if(self.itemExist()==None):
			return "Não há item no Bau"
		#	return(self.personagensAtivos[num].getListItens())
		else:
			return self.bauItem.getSintese()
			#if (type(self.bauItem) is Arma):
					#resposta ="\n#########################################################\n"+"\nItem de Ataque, Nome do item:"+self.bauItem.getNome()+"\nCapacidade de ataque efetivo:"+str(self.bauItem.getAtaque())+"\nCapacidade de defesa:0, Capacidade de Cura:0 \n"+"#########################################################\n"	
					#return resposta
			#if (type(self.bauItem) is Capacidadededefesa):
			#		resposta ="\n#########################################################\n"+"\nItem de Defesa, Nome do item:"+self.bauItem.getNome()+"\nCapacidade de Defesa efetivo:"+str(self.bauItem.getDefesa())+"\nCapacidade de Ataque:0, Capacidade de Cura:0 \n"+"#########################################################\n"	
			#		return resposta
			#if (type(self.bauItem) is Medkit):
			#		resposta ="\n#########################################################\n"+"\nItem de Medical Kit, Nome do item:"+self.bauItem.getNome()+"\nCapacidade de cura efetivo:"+str(self.bauItem.getCura())+"\nCapacidade de Ataque:0, Capacidade de defesa:0\n"+"#########################################################\n"	
			#		return resposta	
			
		#	return "Não é pescador"	

	
	def listItensPescador(self,num):
		if(type(self.personagensAtivos[num]) is Pescador):
			return (self.personagensAtivos[num].getListItens()) # retorna uma string com todos os itens
		else:
			return "Não é um pescador"
	
	def getValorMagias(self,numPersonagem):
		if(numPersonagem > len(self.personagensAtivos)): # caso seja maior retorna None
			return None
		if (type(self.personagensAtivos[numPersonagem]) is Pescador): # caso seja um pescador
			return self.personagensAtivos[numPersonagem].getValorMagias()

	def getIlha(self,numIlha):
		if(len(self.vetorDirecao) < numIlha):
			return None
		else:
			#copy = self.vetorDirecao[numilha]
			return self.vetorDirecao[numIlha]

	def getStatusFromPescador(self,numJogador):
		if(numJogador > len(self.personagensAtivos)):
			return None
		else:
		#	if(type (self.personagensAtivos[numJogador]) is Pescador):
			return(self.personagensAtivos[numJogador].getStatus())
		#	else:
		#		return(self.personagensAtivos[numJogador].getStatus())
	
	# retorna 1 caso somente 1 está vivo caso contrario retorna 0
	def verificaSeSomenteUmEstaVivo(self):
		tam = len(self.personagensAtivos)
		if (tam == 1):
			return 1
		else:
			return 0
	
	# Funcao que acha pescadores com a referencia do objeto
	# Retorna None quando não há personagens ativos na ilha
	# Caso contrario retorna referencia atual
	def refperdida(self,pescadorpont):
		tam = len(self.personagensAtivos) # pega tamanho do vetor de personagens		
		i = 0
		if (tam == 0): # caso o tamanho seja inexistente retorna None
			return None
		else:
			return (self.personagensAtivos.index(pescadorpont)) # retorna a posicao no vetor de pescadores do pescador perdido

	# retira personagem da Ilha
	def retiraPersonagem(self,referencia):
		if(self.personagensAtivos[referencia]!=None):
			del self.personagensAtivos[referencia]
			return referencia
		else:
			return None
		