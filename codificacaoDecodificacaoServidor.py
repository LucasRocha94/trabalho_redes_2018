# -*- coding: utf-8 -*-
import random
import binascii
import string
import difflib

#Esta funcao conta quantos caracteres tera no tamanho da mensagem.
#retorna um numero entre 1 ~ 4, sendo 4 pela limitação de 1024 
#caracteres

def rectameretornadecimal(tamanho):
	if(tamanho > 1 and tamanho < 10):
		return 1
	if(tamanho > 10 and tamanho < 100):
		return 2
	if(tamanho > 100 and tamanho < 1000):
		return 3
	if(tamanho > 1000):
		return 4



#Funcao que descontroi a mensagem de ataque do cliente, retornando os parametros
#argumento uma mensagem no formato de string
# retorna uma tupla com o codigo em formato de numero e os parametros necessarios para o metodo
# BEGG   --> [1,string_nome_do_jogador,numero_do_tipo_de_personagem,ip_do_cliente]
# ATQP   --> [2,id_do_jogador,posicaoatualdopersonagem]
# JOGI   --> [3.. .11,codigo_da_instrucao,id_do_jogador]
# MOVP   --> [12,id_do_jogador,id_ilha_destino]
# USEI   --> [13,id_do_jogador,referencia_no_saco_de_itens]
def decodificaInstrucao(mensagem):
	prefixoMensagem = mensagem[0:4]
	if(prefixoMensagem == 'ATQP'): #tratamento da instrucao de ataque
		mensagemempartes = mensagem.split()
		if (str(len(mensagem)) == mensagemempartes[1]): # compara tamanho com o que foi enviado afim de observar incongruencias
			vetorresposta = [2,mensagemempartes[2],int(mensagemempartes[3])]
			return vetorresposta
		else:
			return None
	if(prefixoMensagem == 'BEGG'):	#tratamento da instrucao de inicio de inicio de conexao
		mensagemempartes = mensagem.split()
		if(str(len(mensagem)) == mensagemempartes[1]): # compara tamanho com o que foi enviado afim de observar incongruencias
			novadivisao = mensagem.split('\r\n')
			vetorresposta = [1, novadivisao[1], int(novadivisao[2]),novadivisao[3]]
			return vetorresposta
		else:
			return None
	if(prefixoMensagem == 'JOGI'):
	 	mensagemempartes = mensagem.split() # separa as informações da mensagem
	 	if(str(len(mensagem))== mensagemempartes[1]): # compara com as informações passadas pela mensagem afim de encontrar incongruencias
	 		if(mensagemempartes[2]=='CATI'): #3
	 			vetorresposta = [3,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='CATM'): #4
	 			vetorresposta = [4,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='DIRC'): #5
	 			vetorresposta = [5,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='EXIN'): #6
	 			vetorresposta = [6,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='GETI'): #7
	 			vetorresposta = [7,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='GETS'): #8	
	 			vetorresposta = [8,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='LSTC'): #9
	 			vetorresposta = [9,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='LSTI'): #10
	 			vetorresposta = [10,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='LSTM'): #11
	 			vetorresposta = [11,mensagemempartes[3]]
	 			return vetorresposta
	 		else:
	 			return None
	 	else:
	 		return None	
	if(prefixoMensagem == 'MOVP'):
		mensagemempartes = mensagem.split() # separa as informações da mensagem
		if(str(len(mensagem)) == mensagemempartes[1]):
			vetorresposta = [12,mensagemempartes[2],int(mensagemempartes[3])]
			return vetorresposta
		else:
			return None
	if(prefixoMensagem == 'USEI'):
		mensagemempartes = mensagem.split() # separa as informações da mensagem
		if(str(len(mensagem))== mensagemempartes[1]):
			vetorresposta = [13,mensagemempartes[2],int(mensagemempartes[3])]
			return vetorresposta
		else :
			return None
	else:
		return None


# Funcao que retorna um vetor de mensagens de respota <2222>
# argumento: tammax mensagem(quantos caracteres terá no maximo a mensagema ser enviada)
# argumento: mensagem original de resposta
def retornaVetorresposta(tammax,mensagem,interacao):
	codigoresposta = '2222'
	vetorresposta = []
	valorinicial = 0
	valorfinal = tammax
	copy =[]
	# adicionar vetorresposta.append() , deletar um obj list.remove(obj)
	i=0
	for i in range(interacao):
		k = i+1
		if((tammax*k) > len(mensagem)): # deve pegar o restante, nao 997 bits
			backup = valorfinal
			resto1 = (len(mensagem)-(i*tammax)) # pega o restante do ultimo pacote
			valorfinal = (i*tammax) + resto1
			copy = mensagem[valorinicial:valorfinal]
			novarespostapar = codigoresposta+' '+' '+'\r\n'+'1'+' '+'\r\n'+str(i)+' '+'\r\n'+'0'+' '+'\r\n'+copy+'\r\n'   # é a ultima mensagem pois nao aproveita todo o tamanho
			tamparcial = len(novarespostapar)
			resto = rectameretornadecimal(tamparcial)
			tamtotalResp = resto + tamparcial
			novoresposta = codigoresposta+' '+str(tamtotalResp)+' '+'\r\n'+'1'+' '+'\r\n'+str(i)+' '+'\r\n'+'0'+' '+'\r\n'+copy+'\r\n'
			vetorresposta.append(novoresposta)
		else:	# deve pegar todo o valor
			copy = mensagem[valorinicial:valorfinal]
			novarespostapar = codigoresposta+' '+' '+'\r\n'+'1'+' '+'\r\n'+str(i)+' '+'\r\n'+str((interacao-1)-i)+' '+'\r\n'+copy+'\r\n'
			tamparcial = len(novarespostapar)
			resto = rectameretornadecimal(tamparcial)
			tamtotalResp = resto + tamparcial
			novoresposta = codigoresposta+' '+str(tamtotalResp)+' '+'\r\n'+'1'+' '+'\r\n'+str(i)+' '+'\r\n'+str((interacao-1)-i)+' '+'\r\n'+copy+'\r\n'
			vetorresposta.append(novoresposta)
			valorinicial = valorfinal
			valorfinal += (tammax)
			
	
	return vetorresposta


# Funcao de resposta do servidor <2222> indicando resposta de comando requisitado
# argumento : mensagem a ser enviada para um jogador
# Retorna uma tupla de mensagens e numero de mensagens, as mensagens sendo inferiores a 1023 caracteres
def respMensagem(mensagem): # mensagem com mais de 1000 caracteres devem ser repartidas
	codigoresposta = '2222'
	if(len(mensagem) <= (100*996)): # maximo de uma mensagem pode conter de caracteres 99500
		if(len(mensagem) <= 997): # cabe em uma mensagem
							 # <2222> + ' ' +' ' +"\r\n"+ flag particionada+" "+'\r\n'+ "numero da mensagem no vetor 2222 possivel (0) nesse caso" +" "+ "\r\n" + (Numero de mensagems de resposta que ainda receberá)+" "+ "\r\n" + mensagem + "\r\n"
			mensagemparcial = codigoresposta+' '+' '+'\r\n'+'0'+' '+'\r\n'+'0'+' '+'\r\n'+'0'+' '+'\r\n'+mensagem+'\r\n'		
			tamparcial = len(mensagemparcial)
			tamtotal   = rectameretornadecimal(tamparcial)
			tamtotal  += tamparcial 
			resposta   =  codigoresposta+' '+str(tamtotal)+' '+'\r\n'+'0'+' '+'\r\n'+'0'+' '+'\r\n'+'0'+' '+'\r\n'+mensagem+'\r\n'		
			vetorresposta = [] 
			vetorresposta.append(resposta)
			return vetorresposta
			# deve ser dividido
		if( len(mensagem) >= 997 and len(mensagem) <= (9*997)): #cabe em unico bit de identificacao de pacote
			ninteracao    = len(mensagem)/997
			intninteracao = int(ninteracao)
			if(intninteracao < ninteracao): # ninteracao eh float necessario adicionar mais um
				interacao  = int(ninteracao)
				interacao += 1
				return (retornaVetorresposta(997,mensagem,interacao))	
			else:
				interacao = int(ninteracao)
				return (retornaVetorresposta(1001,mensagem,interacao))

		if (len(mensagem) > (9*997) and len(mensagem) <= 99*996): # cabe  dois bit de representacao
			ninteracao    = len(mensagem)/996
			intninteracao = int(ninteracao)
			if(intninteracao < ninteracao): # ninteracao eh float necessario adicionar mais um
				interacao  = int(ninteracao)
				interacao += 1
				return (retornaVetorresposta(996,mensagem,interacao))	
			else:
				interacao  = int(ninteracao)
				return (retornaVetorresposta(996,mensagem,interacao))				
		
		if(len(mensagem) > 99*996): # cabe tres bits de representacao
			ninteracao    = len(mensagem)/994
			intninteracao = int(ninteracao)
			if(intninteracao < ninteracao): # ninteracao eh float necessario adicionar mais um
				interacao  = int(ninteracao)
				interacao += 1
				return (retornaVetorresposta(994,mensagem,interacao))	

			else:
				interacao = int(ninteracao)
				return (retornaVetorresposta(994,mensagem,interacao))	
			
	else:
		return None

# Funcao de resposta do servidor <0000> indicando conexao aceita , e que o jogo aguarda mais usuarios
# argumento: id do jogador que foi aceito
# Retorna a resposta pronta para o envio pelo servidor.
def conexaoAceita(playerId):
	codigoresposta = '0000'
	resposta = codigoresposta+' '+playerId+'\r\n'
	return resposta

# Funcao de resposta do servidor <1111> indicando que o jogo iniciou.
# argumento: id do jogador que está atualmente ativo para o envio de comandos
# Retorna a resposta pronta para o envio pelo servidor
def jogoInicio(playerId):
	codigoresposta = '1111' # codigo da mensagem a ser enviado
	resposta = codigoresposta+' '+playerId+'\r\n'
	return resposta 
# Funcao de resposta do servidor <7777> que o jogo ja iniciou, e que está habilitado para o envio de mensagens
# argumento: id do jogador que está atualmente ativo para o envio de comandos
# Retorna a resposta pronta para o envio pelo servidor
def jogoAtivo(playerId):
	codigoresposta = '7777'
	resposta = codigoresposta+' '+playerId+'\r\n'
	return resposta
# Funcao de resposta do servidor <5555> indicando fim de jogo
# argumento : mensagem que indica o ganhador do jogo
# Retorna uma mensagem pronta para o envio pelo servidor.
def fimGame(mensagem):
	codigoresposta = '5555'
	parcialResposta = codigoresposta+' '+' '+'\r\n'+mensagem+' '+'\r\n'
	tamparcial = len(parcialResposta)
	tamtotal = rectameretornadecimal(tamparcial)
	tamtotal+=tamparcial
	resposta = codigoresposta+' '+ str(tamtotal)+' '+'\r\n'+mensagem+' '+'\r\n'
	return resposta

# Funcao de resposta do servidor <9999> indicando a morte do jogador
# argumento : -
# Retorna uma mensagem pronta para o envio pelo servidor.
def mortejogador():
	codigoresposta ='9999'
	resposta = codigoresposta+' '+"O jogador morreu"+'\r\n'
	return resposta



def genRandomwords(numb):
	k = '' # vazio
	for i in range(numb):
		k += random.SystemRandom().choice(string.ascii_uppercase +' '+string.digits)
	return k



# mensagem = genRandomwords(10*990)
# h = respMensagem(mensagem)
# tam = len(h)
# for i in range(tam):
# 	#print(h[i].split('\r\n'))
# 	print(decodificaResposta(h[i]))
# 	#print("tamanho do segmento",len(h[i]))
