# -*- coding: utf-8 -*-
#!/usr/bin/python3
from socket import *
import time
#-----
import threading
import queue
import time
import random
from Mapa import *
#-----
from codificacaoDecodificacaoServidor import *
from codificacaoDecodificacaoCliente import *


class Clienteativo(threading.Thread):
	def __init__(self,conexao,cheio,vazio,atomic,lockjogofuncionando,mapa1,lockSocket_fechados,numb):
		threading.Thread.__init__(self)
		self.conexao = conexao
		#-------------------
		self.cheio= cheio
		self.vazio = vazio
		self.atomic = atomic
		self.lockjogofuncionando = lockjogofuncionando
		self.cadastrado = 0
		self.identificacao = 0
		self.estainiciando = 0 # indica se testes de estavivo devem ser realizado
		self.mapa = mapa1 # soh deve ser usado no inicio
		self.lockSocket_fechados=lockSocket_fechados
		self.threadname = "thread"+str(numb)
		#-------------------
	def run(self):
		# manda para um novo objeto um novo endereço de ip
		global envio # carrega a mensagem que precisa ser decodificada
		global jogofuncionando # informa se eh necessário cadastrar personagem e esperar jogofuncionando
		# estado onde o cliente enviara o <0000>, é cadastrado o cliente e aguardado o começo do jogo 
		# apos isto eh dispara mensagens do tipo <1111>
		n=0
		mandei0000=0
		while(n==0):
			self.lockjogofuncionando.acquire() # verificando se o estado de jogofuncionando não mudou
			if(jogofuncionando == 0): # jogo Não está funcionando 
				self.lockjogofuncionando.release()
				
				if(self.cadastrado == 0): # escuta para receber begg, então o valor self.cadastrado é atualizado para 1
					
					data = self.conexao.recv(1024) # recebe 1024 bytes de informacao
					mensaagemdecodificada = data.decode("latin-1") # decodifica a mensagem
					self.vazio.acquire()
					self.atomic.acquire()
					vetor = decodificaInstrucao(mensaagemdecodificada) # vetor instrucao deve conter begg
					# contendo[1,string_nome_do_jogador,numero_do_tipo_de_personagem,ip_do_cliente]
					resposta = mapa1.addJogador(vetor[1],vetor[2]) # jogador cadastrado
					#resposta[1] contém id do jogador
					self.identificacao = resposta[1] # jogador já cadastrado
					del envio [:] # limpa a lista
					envio.append(resposta[1])
					envio.append(self.conexao)
					envio.append(0)# neste caso é enviado mensagem do tipo <0000>
					mandei0000+=1
					self.cadastrado+=1 # eh adicionado 1 ao cadastro
					self.atomic.release()
					self.cheio.release()
				
				elif(self.cadastrado == 1): # fica no loop ate jogo funcionar
					
					self.vazio.acquire()
					self.atomic.acquire()
					del envio [:] # limpa a lista
					self.atomic.release()
					self.cheio.release()
			
			elif(jogofuncionando == 1): #jogo esta funcionando
				self.lockjogofuncionando.release()
				
				if(self.cadastrado == 0) :  # o jogo ja esta ocorrendo e não foi cadastrado, ele deve receber enviar O <7777>
					data = self.conexao.recv(1024) # recebe 1024 bytes de informacao
					mensaagemdecodificada = data.decode("latin-1") # decodifica a mensagem
					self.vazio.acquire()
					self.atomic.acquire()
					vetor = decodificaInstrucao(mensaagemdecodificada) # vetor instrucao deve conter begg
					# contendo[1,string_nome_do_jogador,numero_do_tipo_de_personagem,ip_do_cliente]
					resposta = mapa1.addJogador(vetor[1],vetor[2]) # jogador cadastrado
					#resposta[1] contém id do jogador
					self.identificacao = resposta[1] # jogador já cadastrado
					del envio [:] # limpa a lista
					envio.append(resposta[1])
					envio.append(self.conexao)
					envio.append(7)# neste caso é enviado mensagem do tipo <7777>
					self.cadastrado+=1 # eh adicionado 1 ao cadastro
					self.atomic.release()
					self.cheio.release()
					n+=1
				# observa se ele enviou a mensagem <0000> caso o tenha feito envia a mensagem <1111> avisando que jogo esta acontecendo
			
				elif(self.cadastrado == 1):
					
					if(mandei0000==0): # ele soh deve liberar o jogo para o usuario
						self.vazio.acquire()
						self.atomic.acquire()
						del envio [:] # limpa a lista
						self.atomic.release()
						self.cheio.release()
						n+=1	
					elif(mandei0000==1): # ele deve mandar 1111
						self.vazio.acquire()
						self.atomic.acquire()
						del envio [:] # limpa a lista
						time.sleep(1)
						envio.append(self.identificacao)
						envio.append(self.conexao)
						envio.append(1)# neste caso é enviado mensagem do tipo <0000>
						self.atomic.release()
						self.cheio.release()
						n+=1
		
					
		global sockets_fechados				
					
	#	exit()	
		n=1	
		while n==1:
		# Recebe data enviada pelo cliente
			self.lockSocket_fechados.acquire()
			if self.conexao in sockets_fechados: # termina com a thread
				n = 2
				self.lockSocket_fechados.release()
				
			else:
				self.lockSocket_fechados.release()	
				data = self.conexao.recv(1024) # recebe 1024 bytes de informacao
				mensaagemdecodificada = data.decode("latin-1")
				if(mensaagemdecodificada!=None):
					backup = decodificaInstrucao(mensaagemdecodificada) # decodifica a fim de observar se é exin
					if(backup==None):
						self.vazio.acquire()
						self.atomic.acquire()
						del envio[:]
						self.atomic.release()
						self.cheio.release()
					elif(backup[0]==6): # É exin
						self.lockSocket_fechados.acquire()
						time.sleep(3)
						sockets_fechados.append(self.conexao) # adiciona o socket recem fecha a lista de fechados
						self.conexao.close() # fecha conexao
						self.lockSocket_fechados.release()
						self.vazio.acquire()
						self.atomic.acquire()
						del envio[:]
						# codigo para a verificacao se o personagem existe, caso exista ele é retirado
						envio.append(6) 
						envio.append(5)
						envio.append(5)
						envio.append(self.identificacao)
						# ---------------------------------------------------------------------------
						self.atomic.release()
						self.cheio.release()
						n=2
					elif(backup[0]!=6):	
					#	print(mensaagemdecodificada)
						self.vazio.acquire()
						self.atomic.acquire()
						del envio [:] # limpa a lista
						#------
						envio.append(mensaagemdecodificada) # envia string decodificada e endereco (socket)
						envio.append(self.conexao)
						#------
						self.atomic.release()
						self.cheio.release()
						# envia envio
						# recebe resposta
				if not data: break 
		print("Encerrada: ",self.threadname)
		self.conexao.close() # fecha a conexão pois thread encerrada
		return None

class Cons(threading.Thread):
	def __init__(self,mapa,cheio,vazio,atomic,lockSocket_fechados,locknumbconections):
		threading.Thread.__init__(self)
		self.mapa = mapa
		self.cheio= cheio
		self.vazio = vazio
		self.atomic = atomic
		self.lockSocket_fechados = lockSocket_fechados
		self.locknumbconections = locknumbconections
	def run(self):
		global envio
		global sockets_fechados			
		global conections
		continua_consumir=0
		while(continua_consumir==0):
		#	print("Ouvindo!!!")
			self.locknumbconections.acquire()
			self.lockSocket_fechados.acquire()
			if(len(conections) == len(sockets_fechados)): # se todas as conexoes foram fechadas termina o programa
				continua_consumir+=1
				print('Thread do consumidor encerrada')
				self.locknumbconections.release()
				self.lockSocket_fechados.release()
			else:
				self.locknumbconections.release()
				self.lockSocket_fechados.release()
				self.cheio.acquire()
				self.atomic.acquire()
				if(len(envio)==2): # jogo funcionando
					message = identificacaodeCaminho(self.mapa,envio[0]) # entrada mapa, mensagem, retorna mensagem pronta pra envio
					tam = len(message)
					if(tam>1): # manda uma serie de mensagens
						for i in range(tam):
							final = bytes(message[i],"latin-1")
							envio[1].send(final)
							time.sleep(1)		
						self.atomic.release()	
						self.vazio.release()
						time.sleep(0.25)	
					else:
						final = bytes(message[0],"latin-1")
					#	print(message[0])
						envio[1].send(final) # envia a mensagem
						mensagem = message[0]
						if(mensagem[0]=='5'): # está enviando um tipo de mensagem do tipo <5555> logo ele fecha a conexao e adiciona o socket fechado ao vetor
						#	envio[1].close() # envia pedido de fechamento de conexão
						#	print("Saiu 230")
							time.sleep(3) # dorme um tempo
							#self.envio[1].shutdown()
						#	envio[1].close()
							self.lockSocket_fechados.acquire()
							sockets_fechados.append(envio[1]) # adiciona na lista dos fechados
							self.lockSocket_fechados.release()
						#	print("Fechou o socket da conexao!!!")
							self.atomic.release()	
							self.vazio.release()
							
						else:	
							self.atomic.release()	
							self.vazio.release()
							time.sleep(0.25)
					# recebe no envio = mensagem em str e ip para envio de resultado
					#decodificainstrucao
				if(len(envio)==3): # jogador deve ser cadastrado ou sera disparado mensagens de <1111>
					# caso envio[2] = 0 deve enviar a mensagem ao <0000>
					if(envio[2]==0): # deve enviar uma mensagem do tipo <0000> ,envio[0] contém id do jogador adicionado
						mensagem = conexaoAceita(envio[0])
						respostapronta = bytes(mensagem,"latin-1")
						envio[1].send(respostapronta)
					#	print(respostapronta)
						self.atomic.release()	
						self.vazio.release()
					if(envio[2]==1): # deve enviar uma mensagem do tipo <1111>
					# caso envio[2] = 1 deve receber envio = [self.identificacao,self.endereco,1]
					# e deve utilizar o self.identificacao para enviar a mensagem correta 
						mensagem = jogoInicio(envio[0])
						respostapronta = bytes(mensagem,"latin-1")
					#	print(respostapronta)
						envio[1].send(respostapronta)
						self.atomic.release()	
						self.vazio.release()
					if(envio[2]==7): # deve enviar uma mensagem do tipo <7777>
						mensagem = jogoAtivo(envio[0])
						respostapronta = bytes(mensagem,"latin-1")
					#	print(respostapronta)
						envio[1].send(respostapronta)
						self.atomic.release()	
						self.vazio.release()
				
				if(len(envio)==0):
					self.atomic.release()	
					self.vazio.release()
				
				if(len(envio)==4): # verifica se personagem foi retirado caso contrário ele o retira
					estavivo = mapa1.verificavida(envio[3]) # retorna 0 caso vivo retorna 1 caso morto
					if(estavivo == 0): # está morto
						self.atomic.release()	
						self.vazio.release()

					else:
						realizaInstrucoes(envio,self.mapa) # envia a referencia do personagem para a retirada
						self.atomic.release()	
						self.vazio.release()
		return None

#@mapa = mapa do jogo
#@mensagem = a mensagem que esta decodificada e vai ser interpretada
#Funcao que realiza a checagem: se jogador está vivo, se há vencedores, modifica mapa e prepara a resposta apos modificao
#Caso algumas das duas checagens de positivo dispara <5555>,<9999>
#Caso não haja não haja morrido e nenhum vencedor na partida é modificado o mapa e é  checado se a resposta é None
#Caso seja neste caso ele prepara a <9999>,Caso contrário ele retorna uma mensagem tipo <2222> com o retorno do comando
def identificacaodeCaminho(mapa1,mensagem):
	#--------------------------------------------
		# vencedor foi encontrado
	vencedor = mapa1.verificafimdejogo()
	if(vencedor[0] == None): # Encerra o Jogo , foi detectado que temos um vencedor, eh disparado a mensagem de <5555>
		quase = fimGame(vencedor[1])
		vectorresposta =[]
		vectorresposta.append(quase)
		#final = bytes(quase,"latin-1")
		return                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       (vectorresposta) # retorna mensagem pronta para envio
	#--------------------------------------------
	else: # Jogo ainda continua
		# a instrucao chega atraves 
		vetorres  = decodificaInstrucao(mensagem) # decodifica a mensagem a fim de obter a instrucao correta para o usuario
		##################################3
	#	print("Linha 287: ",vetorres)
		estavivo = mapa1.verificavida(vetorres[1]) # retorna 0 caso vivo retorna 1 caso morto
		if(estavivo == 0): # está morto
			quase = mortejogador()
			vectorresposta = []
			vectorresposta.append(quase)
			return vectorresposta
		else: # está vivo
			mensagem1 = realizaInstrucoes(vetorres,mapa1) # tentativa de realizar a instrucao
		#	print("Resultado linha 289:",mensagem1)
			if(mensagem1 == None or mensagem1[0] == None): #Jogador Morreu, # prepara a mensagem <9999>
				quase = mortejogador()
				vectorresposta =[]
				vectorresposta.append(quase)
				return vectorresposta
			else: #teremos dois tipos de mensagens <7777> retornando id e <2222> retornando um vetor de mensagem
				if(vetorres[0]==1): # indicando que o usuário gostaria de se juntar ao jogo
					quase = jogoAtivo(mensagem1[1])
					vectorresposta =[]
					vectorresposta.append(quase)
					return vectorresposta	
				else: # eh uma mensagem tipo <2222>
					quase = respMensagem(mensagem1[1])
					return quase # ja é uma lista
	


# Funcao que traduz protocolo decodificado em vetores para instrucoes no MAPA
# Retorna um resultado [tipoderesposta,mensagem]
# tipoderesposta é se vais ser um 0000~9999
# mensagem eh o que o jogo comunicou
def realizaInstrucoes(vetorinstrucao,mapagame):
	if(vetorinstrucao[0]==1): # Pedido de addJogador
		return(mapagame.addJogador(vetorinstrucao[1],vetorinstrucao[2])) # adiciona o jogador no Mapa, nome e tipo de personagem
	
	elif(vetorinstrucao[0]==2): # Pedido de formular um ataque
		return(mapagame.batalhaPersonagensFromIlha(vetorinstrucao[1],vetorinstrucao[2]))
	
	elif(vetorinstrucao[0]==3): # Pega um item da ilha
		return(mapagame.getItemPescador(vetorinstrucao[1]))

	elif(vetorinstrucao[0]==4): # Coletar uma magia da ilha
		return(mapagame.getMagiafromIlha(vetorinstrucao[1]))

	elif(vetorinstrucao[0]==5): # Lista todas as direcoes possiveis a partir da ilha
		return(mapagame.genListadirecoesdaIlha(vetorinstrucao[1]))

	elif(vetorinstrucao[0]==6): # Pede o encerramento da conexao
		return(mapagame.retiraJogador(vetorinstrucao[3]))
		
		#return(mapagame.getStatusFromPescadorDeIlha(vetorinstrucao[1]))
	elif(vetorinstrucao[0]==7): # Pede um relatorio dos personagens da ilha
		return(mapagame.getListdePersonagensfromIlha(vetorinstrucao[1]))

	elif(vetorinstrucao[0]==8): # Pede um relatorio atual da situação atual do jogador
		return(mapagame.getStatusFromPescadorDeIlha(vetorinstrucao[1]))

	elif(vetorinstrucao[0]==9): # Pede uma lista de todos os itens do jogador
		return(mapagame.listarItensDeUmPescador(vetorinstrucao[1]))

	if(vetorinstrucao[0]==10): # Pede uma lista completa de todos os itens presentes na ilha
		return(mapagame.listarItensDeUmaIlha(vetorinstrucao[1]))
		
	if(vetorinstrucao[0]==11): # Pede a informação se existe alguma magia presente na ilha atual do jogador
		return(mapagame.listaMagiaIlha(vetorinstrucao[1]))

	if(vetorinstrucao[0]==12):	# mudar de ilha
		return(mapagame.changePersonagemDeIlha(vetorinstrucao[1],vetorinstrucao[2]))

	if(vetorinstrucao[0]==13):	# Usar um item
		return(mapagame.changePersonagemDeIlha(vetorinstrucao[1],vetorinstrucao[2]))
#-------------------------------------------------------------------
#Cria um mapa
# mapa1 = Mapa() # gera o mapa
# mapa1.genMapaRandon(10)
# player1 = mapa1.addJogador("Claudir",3)
# vector = imprimiMenuRetornaopcoes() # vetor resposta composto por 1 ou mais opcoes
# mensagem = identificaOpcaoConstroiamensagem(vector,player1[1]) # retorna a mensagem completa , pronta para envio
# print(mensagem)

# l = mensagem.decode("latin-1")
# vector1  = decodificaInstrucao(l)
# mensagem1 = realizaInstrucoes(vector1,mapa1)
# print(mensagem1[1])
# exit()

#exit()
# Funcao que recebe o resultado parcial do mapa da realizaInstrucoes()
# Retornando um vetor de mensanges prontos para envio
#def criaMensagemResposta(resultadoparcial):


# Cria um jogador novo, recebe a mensagem que informa que deseja se conectar
# parametro jaestafuncionando = 1, o jogo ja está rodando e alguem deseja se conectar, então ele envia <7777>
# jaestafuncionando = 0, o jogo ainda aguarda jogadores, então ele envia <0000>
# retorna a mensagem que conexao pronta para o usuário
def criandocliente(mensagemcliente1,mapa1,jaestafuncionando):
	mensagemcliente = mensagemcliente1.decode("latin-1")  
	mensagem = decodificaInstrucao(mensagemcliente) # decodifica instrucao em vetor
	retornodocomando = realizaInstrucoes(mensagem,mapa1) # realiza no mapa os comandos e retorna a mensagem
	# Jogador adicionado ao jogo
	if(jaestafuncionando==0):
		quasepronto = conexaoAceita(retornodocomando[1]) # mensagem<0000>
		pronto  = bytes(quasepronto,"latin-1")
		vetorresposta = []
		vetorresposta.append(pronto)
		vetorresposta.append(retornodocomando[1]) # codigo hexa
		return vetorresposta
	if(jaestafuncionando==1):
		quasepronto = jogoAtivo(retornodocomando[1]) # mensagem<0000>
		pronto      = bytes(quasepronto,"latin-1")
		vetorresposta = []
		vetorresposta.append(pronto)
		vetorresposta.append(retornodocomando[1]) # codigo hexa
		return vetorresposta	
	else:
		return None

# Ativa os jogadores que receberam a mensagem <0000> e aguardam inicio de jogo
# desta forma ele envia a mensagem <1111> informando que a thread já está ouvindo a conexao
# e que é possivel o envio de instrucoes
# ele percorre conection ateh 2, que eh o numero de clintes inseridos
# utilizando os ids em hexa para a composicao das mensagens e envio das respectivos
def ativaplayers(conection,identificacoes):
	for i in range(2):
		preparamensagem = jogoInicio(identificacoes[i])
		mensagemfeita = bytes(preparamensagem,"latin-1")
		conection[1].send(mensagemfeita)
	return None

if __name__=='__main__':
	#Cria um mapa
	mapa1 = Mapa() # gera o mapa
	mapa1.genMapaRandon(10)
	# estrutura que guarda todos os hexas
	listauser = []
	# Cria o nome do host
	iplocal = ''

	# Utiliza este número de porto
	portaescutada = 9999


	# cria o objeto do socket
	sockservidor = socket(AF_INET, SOCK_STREAM)



	# vinculacao do servidor com a porta 
	sockservidor.bind((iplocal,portaescutada))

	sockservidor.listen() # ouvindo 
	############################
		# tres semaforos resposaveis pelo produtor consumidor 
	cheio  = threading.Semaphore()
	vazio  = threading.Semaphore()
	atomic = threading.Semaphore()
	cheio.acquire()
	############################
	funcionandojogo  = threading.Lock()# lock que funciona controlando quem utizara acessara jogofuncionando
	############################
	lockSocket_fechados = threading.Lock()   # controla o acesso a variavel global socket_fechados
	locknumbconections = threading.Lock()    # controla acesso ao consumidor da variarel conections
	############################
	envio    = []  # envio é iniciado
	jogofuncionando=0
	############################

	numbplayer = 0
	begin = 0 
	conections =[]		# informa as conexoes ativas
	###############################################################################################
	sockets_fechados =[] # variavel que recebe os sockets que já foram fechados pela conexao
	conections = [] # guarda os objetos de conecao de todos os clientes
	###############################################################################################
	salvaidsdosprimeiros=[] # array que guarda os ids dos dois primeiros clientes a se conectarem
	funcionandoojogo=0
	maxplayer = 0
	condicao = 0
	NUMMAXPLAYERS = 2 # ajuste de o maximo de jogadores possiveis
	while(condicao==0):
		if(maxplayer<NUMMAXPLAYERS): #1,2,3
			# aceita a conecao e o endereço
			conection, adress = sockservidor.accept() # conection representa objetos de recebimento e envio do socket
			print("conectado",adress)
			numbplayer += 1
			locknumbconections.acquire()
			conections.append(conection) # adiciona conexao ao vetor de conexoes
			locknumbconections.release()
			maxplayer+=1
			#-------------------------------------------------------------------------------
				#cria threads
			if(numbplayer == 1):
				novocliente = Clienteativo(conection,cheio,vazio,atomic,funcionandojogo,mapa1,lockSocket_fechados,maxplayer)		
				consumidor = Cons(mapa1,cheio,vazio,atomic,lockSocket_fechados,locknumbconections)
				novocliente.start()
				consumidor.start()
			else:
				if(numbplayer >= 2): # eh repassado para todos
					novocliente = Clienteativo(conection,cheio,vazio,atomic,funcionandojogo,mapa1,lockSocket_fechados,maxplayer)		
					novocliente.start()
					funcionandojogo.acquire() # verificando se o estado de jogofuncionando não mudou
					if(jogofuncionando==0):
						jogofuncionando+=1
						funcionandojogo.release()				
					else:
						funcionandojogo.release()				
				#novocliente.start()
		else:
			print("Lotacao Maxima")
			condicao+=1


