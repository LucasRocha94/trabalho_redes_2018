#!/usr/bin/python3
# -*- coding: utf-8 -*-
#ok
import random
import sys
# from Capacidadededefesa import * # <- referencia da classe
from Item import *

class Capacidadededefesa(Item):
	def __init__(self,nome,mobilidade,impenetrabilidade):
		super().__init__(nome)
		self.mobilidade = mobilidade
		self.impenetrabilidade = impenetrabilidade
	
	def getNome(self):
		return super().getNome()

	def getDefesa(self):
		return(self.mobilidade * self.impenetrabilidade)
	
	def repassaDano(self,dano):
		value = self.getDefesa()
		if(value <= dano):
			self.impenetrabilidade = 0
		if(value > dano):
			calculonovovalordefesa = self.getDefesa() -  dano
			self.mobilidade = 1
			self.impenetrabilidade = calculonovovalordefesa

	def getSintese(self):
		return "\n#########################################################\n"+"\nItem de Defesa, Nome do item:"+self.getNome()+"\nCapacidade de Defesa efetivo:"+str(self.getDefesa())+"\nCapacidade de Ataque:0, Capacidade de Cura:0 \n"+"#########################################################\n"