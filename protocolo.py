# -*- coding: utf-8 -*-
import random
import binascii
import string
import difflib
#Esta funcao conta quantos caracteres tera no tamanho da mensagem.
#retorna um numero entre 1 ~ 4, sendo 4 pela limitação de 1024 
#caracteres

def rectameretornadecimal(tamanho):
	if(tamanho > 1 and tamanho < 10):
		return 1
	if(tamanho > 10 and tamanho < 100):
		return 2
	if(tamanho > 100 and tamanho < 1000):
		return 3
	if(tamanho > 1000):
		return 4

def escolhadainstrucaoaserenviada(escolhaistrucao):
	if(escolhaistrucao == 1): # Pegar um item da ilha
		escolha = 'CATI'
		return escolha
	if(escolhaistrucao == 2): # Coletar uma magia da ilha
		escolha = 'CATM'
		return escolha
	if(escolhaistrucao == 3): # lista todas as direcoes possiveis a partir da ilha
		escolha = 'DIRC'
		return escolha
	if(escolhaistrucao == 4): # Pede o encerramento da conexao
		escolha = 'EXIN'
		return escolha
	if(escolhaistrucao == 5): # Pede um relatorio dos personagens da ilha
		escolha = 'GETI'
		return escolha
	if(escolhaistrucao == 6): # Pede um relatorio atual da situação atual do jogador
		escolha = 'GETS'
		return escolha
	if(escolhaistrucao == 7): # Pede uma lista de todos os itens do jogador
		escolha = 'LSTC'
		return escolha
	if(escolhaistrucao == 8): # Pede uma lista completa de todos os itens presentes na ilha
		escolha = 'LSTI'
		return escolha
	if(escolhaistrucao == 9): # Pede a informação se existe alguma magia presente na ilha atual do jogador
		escolha = 'LSTM'
		return escolha
	else:				# Erro o que foi pedido não foi encontrado, instrucao sera ignorada
		escolha = 'ERRO'
		return escolha	



# Funcao que constroi a mensagem de Ataque , no lado cliente
# argumento um numero hexa playerID
# argumento um numero decimal inimigoposicao
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaATQP(inimigoposicao,playerID):
	codigoDainstrucao = 'ATQP' # codigo
	tamtotal_da_mensagem = 0   #0~1024
	strininimigo = str(inimigoposicao)
	mensagemConst = codigoDainstrucao + ' '+' ' +'\r\n' + playerID +' '+ '\r\n'+strininimigo+'\r\n'
	tammensagemparcial = len(mensagemConst)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espacao em branco
	#finalcode = codigoDainstrucao + ' '+ str(tamtotal_da_mensagem) +' '+'\r\n' + Idplayer.decode("latin_1") +' '+'\r\n'+strininimigo+'\r\n'
	finalcode = codigoDainstrucao + ' '+ str(tamtotal_da_mensagem) +' '+'\r\n' + playerID +' '+'\r\n'+strininimigo+'\r\n'
	return finalcode

# Funcao que constroi a mensagem de begin conecao , no lado cliente
# argumento uma string entre 5~32 caracteres
# argumento um numero decimal entre 1~3
# um endereço ip
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaBEGG(strNamePlayer,numTipoDoJogador,enderecoip):
	codigoDainstrucao = 'BEGG' # codigo
	tamtotal_da_mensagem = 0   #0~1024
	mensagemparcial = codigoDainstrucao+' '+' '+'\r\n'+strNamePlayer+' '+'\r\n'+str(numTipoDoJogador)+' ' +'\r\n'+str(enderecoip)+'\r\n' 		
	tammensagemparcial = len(mensagemparcial)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espaco em branco
	finalcode = codigoDainstrucao+' '+str(tamtotal_da_mensagem)+' '+'\r\n'+strNamePlayer+' '+'\r\n'+str(numTipoDoJogador)+' ' +'\r\n'+str(enderecoip)+'\r\n' 		
	return finalcode


# Funcao que constroi a mensagem JOGI, mensagem que tem a capacidade de encapsular muitas mensagens
# argumento escolhaistrucao que o jogador deseja realizar str
# argumento um numero hexa playerID
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaJOGI(escolhaistrucao,playerID):
	codigoDainstrucao = 'JOGI'
	strescolha =  escolhadainstrucaoaserenviada(escolhaistrucao)
	mensagemConst = codigoDainstrucao + ' '+' ' +'\r\n' +strescolha+' '+ '\r\n'+playerID+' '+'\r\n'
	tammensagemparcial = len(mensagemConst)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espaco em branco
	finalcode = codigoDainstrucao + ' '+str(tamtotal_da_mensagem)+' ' +'\r\n' +strescolha+' '+ '\r\n'+playerID+' '+'\r\n'
#	print(finalcode)
	return finalcode

# Funcao que constroi a mensagem MOVP, mensagem que comunica o comando do usuário de mudar de ilha
# argumento um numero em hexa playerID
# argumento a identificacao da saida da ilha do usuário
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaMOVP(playerID,caminhoIlha):
	codigoDainstrucao = 'MOVP'
	mensagemConst 	  = codigoDainstrucao + ' '+' ' +'\r\n' +playerID+' '+ '\r\n'+str(caminhoIlha)+' '+'\r\n'
	tammensagemparcial = len(mensagemConst)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espaco em branco
	finalcode = codigoDainstrucao + ' '+str(tamtotal_da_mensagem)+' ' +'\r\n' +playerID+' '+ '\r\n'+str(caminhoIlha)+' '+'\r\n'
	return finalcode


# Funcao que constroi a mensagem USEI, mensagem que comunica o comando do usuário de utilizar um item do seu saco de itens
# argumento um numero em hexa do playerID
# argumento o codigo que identifica o item dentro do saco de itens
# retorno eh uma string que eh a mensagem do cliente pronta para envio
def codificaUSEI(playerID,codigoItem):
	codigoDainstrucao = 'USEI'
	mensagemConst 	  = codigoDainstrucao + ' '+' ' +'\r\n' +playerID+' '+ '\r\n'+str(codigoItem)+' '+'\r\n'
	tammensagemparcial = len(mensagemConst)
	resto = rectameretornadecimal(tammensagemparcial)
	tamtotal_da_mensagem = resto + tammensagemparcial # tamanho dos numeros em caracteres somado a um espaco em branco
	finalcode = codigoDainstrucao + ' '+str(tamtotal_da_mensagem)+' ' +'\r\n' +playerID+' '+ '\r\n'+str(codigoItem)+' '+'\r\n'
	return finalcode

#Funcao que descontroi a mensagem de ataque do cliente, retornando os parametros
#argumento uma mensagem no formato de string
# retorna uma tupla com o codigo em formato de numero e os parametros necessarios para o metodo
# BEGG   --> [1,string_nome_do_jogador,numero_do_tipo_de_personagem,ip_do_cliente]
# ATQP   --> [2,id_do_jogador,posicaoatualdopersonagem]
# JOGI   --> [3.. .11,codigo_da_instrucao,id_do_jogador]
# MOVP   --> [12,id_do_jogador,id_ilha_destino]
# USEI   --> [13,id_do_jogador,referencia_no_saco_de_itens]
def decodificaInstrucao(mensagem):
	prefixoMensagem = mensagem[0:4]
	if(prefixoMensagem == 'ATQP'): #tratamento da instrucao de ataque
		mensagemempartes = mensagem.split()
		if (str(len(mensagem)) == mensagemempartes[1]): # compara tamanho com o que foi enviado afim de observar incongruencias
			vetorresposta = [2,mensagemempartes[2],int(mensagemempartes[3])]
			return vetorresposta
		else:
			return None
	if(prefixoMensagem == 'BEGG'):	#tratamento da instrucao de inicio de inicio de conexao
		mensagemempartes = mensagem.split()
		if(str(len(mensagem)) == mensagemempartes[1]): # compara tamanho com o que foi enviado afim de observar incongruencias
			novadivisao = mensagem.split('\r\n')
			vetorresposta = [1, novadivisao[1], int(novadivisao[2]),novadivisao[3]]
			return vetorresposta
		else:
			return None
	if(prefixoMensagem == 'JOGI'):
	 	mensagemempartes = mensagem.split() # separa as informações da mensagem
	 	if(str(len(mensagem))== mensagemempartes[1]): # compara com as informações passadas pela mensagem afim de encontrar incongruencias
	 		if(mensagemempartes[2]=='CATI'): #3
	 			vetorresposta = [3,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='CATM'): #4
	 			vetorresposta = [4,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='DIRC'): #5
	 			vetorresposta = [5,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='EXIN'): #6
	 			vetorresposta = [6,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='GETI'): #7
	 			vetorresposta = [7,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='GETS'): #8	
	 			vetorresposta = [8,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='LSTC'): #9
	 			vetorresposta = [9,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='LSTI'): #10
	 			vetorresposta = [10,mensagemempartes[3]]
	 			return vetorresposta
	 		if(mensagemempartes[2]=='LSTM'): #11
	 			vetorresposta = [11,mensagemempartes[3]]
	 			return vetorresposta
	 		else:
	 			return None
	 	else:
	 		return None	
	if(prefixoMensagem == 'MOVP'):
		mensagemempartes = mensagem.split() # separa as informações da mensagem
		if(str(len(mensagem)) == mensagemempartes[1]):
			vetorresposta = [12,mensagemempartes[2],int(mensagemempartes[3])]
			return vetorresposta
		else:
			return None
	if(prefixoMensagem == 'USEI'):
		mensagemempartes = mensagem.split() # separa as informações da mensagem
		if(str(len(mensagem))== mensagemempartes[1]):
			vetorresposta = [13,mensagemempartes[2],int(mensagemempartes[3])]
			return vetorresposta
		else :
			return None
	else:
		return None

#------------------------------------------------------------------------------------------------


#	if(prefixoMensagem == 'USEI'): 		



inimy = hex(111)
play  = hex(120)
#c = codificaATQP(12,inimy)
#k = decodificaInstrucao(c)
#c = codificaBEGG("Locas 12 dasd",1,"192.0.0.0")
#c = codificaJOGI(2,inimy)
#c = codificaMOVP(inimy,1)
c = codificaUSEI(inimy,1)
k = decodificaInstrucao(c)
print(k)


# Funcao de resposta do servidor <0000> indicando conexao aceita , e que o jogo aguarda mais usuarios
# argumento: id do jogador que foi aceito
# Retorna a resposta pronta para o envio pelo servidor.
def conexaoAceita(playerId):
	codigoresposta = '0000'
	resposta = codigoresposta+' '+playerId+'\r\n'
	return resposta

# Funcao de resposta do servidor <1111> indicando que o jogo iniciou.
# argumento: id do jogador que está atualmente ativo para o envio de comandos
# Retorna a resposta pronta para o envio pelo servidor
def jogoInicio(playerId):
	codigoresposta = '1111' # codigo da mensagem a ser enviado
	resposta = codigoresposta+' '+playerId+'\r\n'
	return resposta 
# Funcao de resposta do servidor <7777> que o jogo ja iniciou, e que está habilitado para o envio de mensagens
# argumento: id do jogador que está atualmente ativo para o envio de comandos
# Retorna a resposta pronta para o envio pelo servidor
def jogoAtivo(playerId):
	codigoresposta = '7777'
	resposta = codigoresposta+' '+playerId+'\r\n'
	return resposta

# Funcao que retorna um vetor de mensagens de respota <2222>
# argumento: tammax mensagem(quantos caracteres terá no maximo a mensagema ser enviada)
# argumento: mensagem original de resposta
def retornaVetorresposta(tammax,mensagem,interacao):
	codigoresposta = '2222'
	vetorresposta = []
	valorinicial = 0
	valorfinal = tammax
	copy =[]
	# adicionar vetorresposta.append() , deletar um obj list.remove(obj)
	i=0
	for i in range(interacao):
		k = i+1
		if((tammax*k) > len(mensagem)): # deve pegar o restante, nao 997 bits
			backup = valorfinal
			resto1 = (len(mensagem)-(i*tammax)) # pega o restante do ultimo pacote
			valorfinal = (i*tammax) + resto1
			copy = mensagem[valorinicial:valorfinal]
			novarespostapar = codigoresposta+' '+' '+'\r\n'+'1'+' '+'\r\n'+str(i)+' '+'\r\n'+copy+'\r\n'
			tamparcial = len(novarespostapar)
			resto = rectameretornadecimal(tamparcial)
			tamtotalResp = resto + tamparcial
			novoresposta = codigoresposta+' '+str(tamtotalResp)+' '+'\r\n'+'1'+' '+'\r\n'+str(i)+' '+'\r\n'+copy+'\r\n'
			vetorresposta.append(novoresposta)
		else:	# deve pegar todo o valor
			copy = mensagem[valorinicial:valorfinal]
			novarespostapar = codigoresposta+' '+' '+'\r\n'+'1'+' '+'\r\n'+str(i)+' '+'\r\n'+copy+'\r\n'
			tamparcial = len(novarespostapar)
			resto = rectameretornadecimal(tamparcial)
			tamtotalResp = resto + tamparcial
			novoresposta = codigoresposta+' '+str(tamtotalResp)+' '+'\r\n'+'1'+' '+'\r\n'+str(i)+' '+'\r\n'+copy+'\r\n'
			vetorresposta.append(novoresposta)
			valorinicial = valorfinal
			valorfinal += (tammax)
			
	
	return vetorresposta


# Funcao de resposta do servidor <2222> indicando resposta de comando requisitado
# argumento : mensagem a ser enviada para um jogador
# Retorna uma tupla de mensagens e numero de mensagens, as mensagens sendo inferiores a 1023 caracteres
def respMensagem(mensagem): # mensagem com mais de 1000 caracteres devem ser repartidas
	codigoresposta = '2222'
	if(len(mensagem) <= (1001*100)): # maximo de uma mensagem pode conter de caracteres 99500
		if(len(mensagem) <= 1001): # cabe em uma mensagem
			mensagemparcial = codigoresposta+' '+' '+'\r\n'+'0'+' '+'\r\n'+'0'+' '+'\r\n'+mensagem+'\r\n'		
			tamparcial = len(mensagemparcial)
			tamtotal   = rectameretornadecimal(tamparcial)
			tamtotal  += tamparcial 
			resposta   =  codigoresposta+' '+str(tamtotal)+' '+'\r\n'+'0'+' '+'\r\n'+'0'+' '+'\r\n'+mensagem+'\r\n'		
			return resposta
			# deve ser dividido
		if( len(mensagem) >= 1001 and len(mensagem) <= (9*1001)): #cabe em unico bit de identificacao de pacote
			ninteracao    = len(mensagem)/1001
			intninteracao = int(ninteracao)
			if(intninteracao < ninteracao): # ninteracao eh float necessario adicionar mais um
				interacao  = int(ninteracao)
				interacao += 1
				return (retornaVetorresposta(1001,mensagem,interacao))	
			else:
				interacao = int(ninteracao)
				return (retornaVetorresposta(1001,mensagem,interacao))

		if (len(mensagem) > (9*1001) and len(mensagem) <= 99*1000): # cabe  dois bit de representacao
			ninteracao    = len(mensagem)/1000
			intninteracao = int(ninteracao)
			if(intninteracao < ninteracao): # ninteracao eh float necessario adicionar mais um
				interacao  = int(ninteracao)
				interacao += 1
				return (retornaVetorresposta(1000,mensagem,interacao))	
			else:
				interacao  = int(ninteracao)
				return (retornaVetorresposta(1000,mensagem,interacao))				
		
		if(len(mensagem) > 99*1000): # cabe tres bits de representacao
			ninteracao    = len(mensagem)/999
			intninteracao = int(ninteracao)
			if(intninteracao < ninteracao): # ninteracao eh float necessario adicionar mais um
				interacao  = int(ninteracao)
				interacao += 1
				return (retornaVetorresposta(999,mensagem,interacao))	

			else:
				interacao = int(ninteracao)
				return (retornaVetorresposta(999,mensagem,interacao))	
			
	else:
		return None

# Funcao de resposta do servidor <5555> indicando fim de jogo
# argumento : mensagem que indica o ganhador do jogo
# Retorna uma mensagem pronta para o envio pelo servidor.
def fimGame(mensagem):
	codigoresposta = '5555'
	parcialResposta = codigoresposta+' '+' '+'\r\n'+mensagem+' '+'\r\n'
	tamparcial = len(parcialResposta)
	tamtotal = rectameretornadecimal(tamparcial)
	tamtotal+=tamparcial
	resposta = codigoresposta+' '+ str(tamtotal)+' '+'\r\n'+mensagem+' '+'\r\n'
	return resposta

# Funcao de resposta do servidor <9999> indicando a morte do jogador
# argumento : -
# Retorna uma mensagem pronta para o envio pelo servidor.
def mortejogador():
	codigoresposta ='9999'
	resposta = codigoresposta+' '+"O jogador morreu"+'\r\n'
	return resposta


def genRandomwords(numb):
	k = '' # vazio
	for i in range(numb):
		k += random.SystemRandom().choice(string.ascii_uppercase +' '+string.digits)
	return k	
# Funcao do lado cliente que decodifica todas as resposta do servidor
# argumento : resposta do servidor
# Retorna uma mensagem pronta para o cliente
def decodificaResposta(mensagem):
	prefixo = mensagem[0:4]
	if(prefixo=='0000'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		resposta = [0,mensagemparcial[1],"Conexão aceita, aguardando inicio de jogo"]
		return  resposta # retorna um codigo que representa a resposta , a identificacao do jogador, mensagem
	if(prefixo=='1111'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		resposta = [1,mensagemparcial[1],"Jogo iniciou, aguardando comandos !!!"]
		return  resposta # retorna um codigo que representa a resposta , a identificacao do jogador, mensagem
	if(prefixo=='7777'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		resposta = [7,mensagemparcial[1],"Jogo ja iniciou, aguardando comandos !!!"]
		return  resposta # retorna um codigo que representa a resposta , a identificacao do jogador, mensagem
	if(prefixo=='2222'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		if(len(mensagemparcial)<=4): # Não tem white space
			if(int(mensagemparcial[1])==len(mensagem)): #ok não foi 
				resposta = [2,int(mensagemparcial[2]),mensagemparcial[1],mensagemparcial[3],mensagemparcial[4]] # retorna codigo instrucao, id do pacote e mensagem
				return resposta
			else:
				return None
		else: # mensagem possui white space
			novosplitsegundonovalinha = mensagem.split('\r\n')
			if(int(mensagemparcial[1])==len(mensagem)): #ok não foi 
			# 	substring = ''
			# 	for i in range(4,len(mensagemparcial)):
			# 		if(i==len(mensagemparcial)):
			# 			substring +=  (mensagemparcial[i])
			# 		else:
			# 			substring +=  (mensagemparcial[i]+' ')

				substring = novosplitsegundonovalinha[3]
				resposta = [2,int(mensagemparcial[2]),mensagemparcial[1],mensagemparcial[3],substring] # retorna codigo instrucao, id do pacote e mensagem
				return resposta	
			else:
				return None
	if(prefixo=='5555'): # primeiros 4 caracteres da mensagem
		mensagemparcial = mensagem.split() # divide a mensagem em partes
		message = len(mensagemparcial)
		if(int(mensagemparcial[1]) == len(mensagem)): # ok
			respotamensagem = mensagem[10:(len(mensagem)-2)]
			resposta = [5,respotamensagem] # retorna o codigo da resposta e a mensagem
			return resposta
		else :	 # mensagem corrompida
			return None
	if(prefixo=='9999'): # primeiros 4 caracteres da mensagem
		respotamensagem = mensagem[6:(len(mensagem)-2)]
		resposta = [9,respotamensagem]
		return resposta
	else:
		return None

class JuntaMensagem(object):
	def __init__(self,tam,numpacote,mensagem):
		self.tam = tam
		self.numpacote = int(numpacote)
		self.mensagem = mensagem
	def getTam(self):
		return self.tam
	def getnumPacote(self):
		return self.numpacote
	def getMensagem(self):
		return self.mensagem



#s = ''
h = hex(255)
#print(h)
#j = conexaoAceita(h)
#j=jogoInicio(h)
#j=jogoAtivo(h)
#j=fimGame('Maradona venceu!!!')
#j=mortejogador()
#mensagem = genRandomwords(50*100) # gera palavras aleatorias
#print('A mensagem:\n')
#print(mensagem)
# k1 = respMensagem(mensagem) # devolve um vetor contendo todos os pacotes a enviar
# if (k1 ==1):
# 	k = decodificaResposta(k1)
# else:
# 	copy = []
# 	stream = ''
# 	for i in range(len(k1)):
# 		copy.append(decodificaResposta(k1[i]))
# 		catcher =  copy[i]
# 		stream += catcher[4]
		
# 	print(len(stream))
# 	print('\n')
# 	print(len(mensagem))
	
# 	if(stream == mensagem):
# 		print('Parabens')
#print(k)


# k = len(j)
# i=0
# a = []
# total = ''
# if(type(j[0]) == list):
# 	for i in range(k):
# 		copy = decodificaResposta(j[i])
# 		if(copy == None):
# 			print('problem')
# 			exit()
# 		l = JuntaMensagem(copy[2],copy[3],copy[4])
# 		a.append(l)
# 		if(a[i].getnumPacote()==i):
# 			total+=a[i].getMensagem()

# else:
# 	copy = decodificaResposta(j)
# 	total = copy[4]
# 	print(total)	
#tam = len(stream)
#	s+=stream[20:tam-2] 
	#print(stream[26:tam-2])
#print('tamanho',len(j))	
#print(mensagem)
#print('\n')
#print(stream)
#	tam = len(stream)
	#print(stream[20:tam])
#	s+=stream[20:tam-2]
#exit()
# if(total == mensagem):
#  	print("É a mesma string")
#else:
# 	print(s)
# 	print(mensagem)

#print(len(s))
exit()


# 		print(' \n')
#g = decodificaResposta(j)
#print(g)
