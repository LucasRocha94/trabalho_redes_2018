#!/usr/bin/python3
# -*- coding: utf-8 -*-
#ok
import random
import sys
from Ilha import *
from Pescador import *
from Personagem import *
from Magia import *
from Item import *
from Medkit import *
from Arma import *
from Capacidadededefesa import *
from Jogador import *

class Mapa(object):
	
	def __init__ (self): 
		self.arquipelago    		   = []
		self.totalnumbmagic 		   = 0 # valor das no jogo 
		self.magiascoletadas		   = 0 # valor das magias coletadas
		self.fimdeJogo 				   = 0
		self.jogadoresAtivos	       = []  # vetor que contêm objeto de coordenadas 
	
		# retorna o numero de ilhas do arquipelago
	def getNumerodeIlhas(self):
		return len(self.arquipelago)

		#adiciona ilha no arquipelago
	def addIlha(self,ilha):
		self.arquipelago.append(ilha)

		#retorna uma lista com o nome de todas as ilhas do arquipelago
	def getListdasIlhas(self):
		i = 0 
		tam = self.getNumerodeIlhas()
		resposta=""
		if(tam > 0):
			for i in range(tam):
				resposta +="\n#################################################\n"+"Ilha com o nome de:"+self.arquipelago[i].getNome()+"\nNumero da ilha: "+str(i)+"\n#################################################\n"
			return resposta
		else:
			return "Não há ilhas no arquipelago"
			
		# gera a magia paras o mapa randomico
	def genMagiaParaIlha(self,num):
		if(num==1):
			magia1 = Magia("Magia do dragao",100)
			self.setnumberofmagic(100)
			return magia1
		if(num==2):
			magia2 = Magia("Magia da serpente",70)
			self.setnumberofmagic(70)
			return magia2
		if(num==3):
			magia3 = Magia("Magia do coelho",90)
			self.setnumberofmagic(90)
			return magia3
		if(num==4):
			magia4 = Magia("Magia do goblin",120)
			self.setnumberofmagic(120)
			return magia4
		if(num==5):
			magia5 = Magia("Magia do gigante",130)
			self.setnumberofmagic(130)
			return magia5
		else:
			return None 
			# gera inimigos locais para as ilhas randomicamente
	def genPirataparaIlha(self,num):	
		if(num==1):
			piratasemolho 	 = Personagem("Pirata sem Olho",20,30,20)
			return piratasemolho
		if(num==2):
			piratanovato 	 = Personagem("Pirata Novato",30,20,10)
			return piratanovato
		if(num==3):
			pirataForte  = Personagem("Pirata Forte",40,30,30) 
			return pirataForte
		else:
			return None

		# gera itens para as ilhas randomicamente
	def genItensparaIlha(self,num):	
		if(num==1):
			bandagem = Medkit("Bandagem",10)
			return bandagem
		if(num==2):
			antibiotico = Medkit("Injeção de Antibiotico",40)
			return antibiotico
		if(num==3):		
			antiinflamatorio  = Medkit("Injeção de Anti-inflamatorio",50)
			return antiinflamatorio
		if(num==4):
			adaga 	  	 = Arma("Adaga",2,8) 		    #16 ataque
			return adaga
		if(num==5):
			punhal 	  	 = Arma("Punhal",3,7)		    #21  ataque
			return punhal
		if(num==6):
			espadacurta  = Arma("Espada Curta",5,6)    #30 ataque
			return espadacurta
		if(num==7):
			espadalonga  = Arma("Espada Longa",7,5)    #35 ataque
			return espadalonga
		if(num==8):
			machadocurto = Arma("Machado Curto",8,5)   #40 ataque
			return machadocurto
		if(num==9):
			machadolongo = Arma("Machado Longo",8,6)   #48 ataque
			return machadolongo
		if(num==10):
			martelocurto = Arma("Martelo Curto",6,9)   #54 ataque
			return martelocurto
		if(num==11):
			martelolongo = Arma("Martelo Longo",6,10)  #60 ataque
			return martelolongo
		if(num==12):
			escudoleve   = Capacidadededefesa("Escudo leve",5,4)    # 20 de defesa
			return escudoleve
		if(num==13):
			escudopesado = Capacidadededefesa("Escudo pesado",4,6)   # 24 de defesa
			return escudopesado
		if(num==14):
			cotademalha  = Capacidadededefesa("Cota de Malha",9,4)   # 36 de defesa
			return cotademalha
		else:
			return None

		# seta a variaval total de magica no jogo
		# conta as magias adicionadas nas ilhas 
	def setnumberofmagic(self,num):
		self.totalnumbmagic += num
		
		#  devolve valor do total de magia
	def getnumberofmagic(self):
		return self.totalnumbmagic
	
	# caso todas tenham sido coletadas retorna 1 , caso contrario retorna 0
	def verificaMagiasColetadas(self):
		if(self.totalnumbmagic == self.magiascoletadas):
			return 1
		else:
			return 0

	def verificafimdejogo(self):
		parcialresposta = self.verificaSeSomenteUmEstaVivo()
	#	indplayer = self.achaReferencia(0) # acha a referencia do jogado 
		if(len(self.jogadoresAtivos)==0):
			indice = None
			message = "Fim de Jogo Ganhador: EMPATE"
			l = [None,message,1]
			return l
		if(parcialresposta == 1):
			indplayer =  self.achaReferencia(self.jogadoresAtivos[0].getId())
			posic = self.jogadoresAtivos[indplayer].getPosicao()
			#message = "Fim de Jogo Ganhador:\n"+self.arquipelago[posic[0]].personagensAtivos[posic[1]].getStatusPescador()
			#print(posic)
			message = "Fim de Jogo Ganhador:\n"+self.arquipelago[posic[0]].personagensAtivos[posic[1]].getStatus()
			
			indice = None
			l = [indice,message,1]
			return l
		parcialresposta1 = self.verificaMagiasColetadas()
		if(parcialresposta1 == 1): # todas as magias foram coletadas
			tam = len(self.jogadoresAtivos)
			if(tam == 1):
				posic = self.jogadoresAtivos[0].getPosicao()
				message = "Fim de Jogo Ganhador:\n"+self.arquipelago[posic[0]].personagensAtivos[posic[1]].getStatus()+self.arquipelago[posic[0]].personagensAtivos[posic[1]].getValorMagias()
				#message = "Fim de Jogo Ganhador:\n"+self.arquipelago[posic[0]].personagensAtivos[posic[1]].getStatusPescador()+self.arquipelago[posic[0]].personagensAtivos[posic[1]].getValorMagias()
				indice = None
				l = [indice,message,1]
				return l
			else:
				i=0
				acuu = 0
				maxvalue = 0
				indicemaximo =0
				for i in range(tam):
					posic = self.jogadoresAtivos[i].getPosicao()
					acuu = self.arquipelago[posic[0]].personagensAtivos[posic[1]].getValorMagias() # pega as magias
					if(maxvalue < acuu):# caso acumular seja maior maxvalue eh atualizado
						maxvalue = acuu
						indicemaximo = i
				posic2 = self.jogadoresAtivos[indicemaximo].getPosicao()
			#	message = "Fim de Jogo Ganhador:\n"+self.arquipelago[posic2[0]].personagensAtivos[posic2[1]].getStatusPescador()+"Valor das Magias coletadas: \n"+self.jogadoresAtivos[i].getValorMagias()
				message = "Fim de Jogo Ganhador:\n"+self.arquipelago[posic2[0]].personagensAtivos[posic2[1]].getStatus()+"Valor das Magias coletadas: \n"+self.jogadoresAtivos[i].getValorMagias()
				indice = None
				l = [indice,message,1]
			return l
		else:
			indice = 0
			message =""
			l = [indice,message]
			return l


	def genMapaRandon(self,NumDeIlhas): # gera mapa randomico, com excessao dos vizinhos das ilhas
		i = 0
		tam = NumDeIlhas
		tam1= NumDeIlhas -1 
		vetorlivresemapontamentos = []
		for i in range(tam): # gera as ilhas , itens, magias  e piratas nas ilhas
			makingnameilha = "Ilha " + str(i) # realiza a concatenacao da posicao do indice com o prefixo ilha
			if(i != tam -1):
				ilharandom = Ilha(makingnameilha)
			if(i == tam-1):
				ilharandom = Ilha("Ilha central") 
			self.addIlha(ilharandom)
			self.arquipelago[i].addItem(self.genItensparaIlha(random.randint(1,14))) # gera um numero entre 1-11 que representam itens a serem sorteados
			self.arquipelago[i].addMagia(self.genMagiaParaIlha(random.randint(1,5))) # gera um numero entre 1-5 que representam magia a serem sorteados
			self.arquipelago[i].addPersonagem(self.genPirataparaIlha(random.randint(1,3)))  # gera um numero entre 1-3 que representam piratas a serem sorteados
		
		#	vetorapontados.append(i) # vetor de numeros que nao apontaram 
		for i in range(tam1):	
			if(i!=tam1-1):
				self.arquipelago[i].addIlha(self.arquipelago[i+1])# ilha 0 -> ilha 1 -> ilha 2 -> ilha 3
				# constroi a ligacao da ilha central
				self.arquipelago[i].addIlha(self.arquipelago[tam-1]) # add ilha central -> ilha atual
				self.arquipelago[tam-1].addIlha(self.arquipelago[i]) # add ilha atual   -> ilha central
				#----------------------------------
			if(i==tam1-1):
				self.arquipelago[i].addIlha(self.arquipelago[0])
				# controi a ligacao da ilha central
				self.arquipelago[i].addIlha(self.arquipelago[tam-1]) # add ilha central -> ilha atual
				self.arquipelago[tam-1].addIlha(self.arquipelago[i]) # add ilha atual   -> ilha central
				#----------------------------------
			#self.geraDirecoesNasIlhas(i,tam)

			# adiciona as opcoes d
	def printOpcao(self):
		string ='\033[48m'+'\033[0m'+'\033[38m'+"Se voce quiser um marujo ser interpretado por um:\n\n"+"\033[31m"+"Marujo: pouca vida(pequena experiencia em combate), arma de pequena capacidade de ataque, grande protecao, caso deseje este personagem digite 1\n\n"+"\033[33m"+"Intendente: algumas justas(experiencia media em combate), arma intermediaria de ataque, protecao media, digite 2\n\n"+ "\033[34m"+"Capitao dos pescadores: muitas justas(experiencia media em combate), arma ataque maximo , protecao minima, digite 3\n"
		return string

		#Retorna um pescador
	
	def typePescador(self,nome,tipo):
		if(tipo == 1): # soldado leve, arma leve, maxima protecao , vida minima
			vida = 50
			numarma = random.randint(4,6)	
			numdefesa = 14
			pescadornovo = Pescador(nome,vida,self.genItensparaIlha(numarma),self.genItensparaIlha(numdefesa))
			return pescadornovo
		if(tipo == 2): # intendente protecao intermediaria e protecao intermediaria, vida intermediaria
			vida = 70
			numarma = random.randint(6,9)	
			numdefesa = 13
			pescadornovo = Pescador(nome,vida,self.genItensparaIlha(numarma),self.genItensparaIlha(numdefesa))
			return pescadornovo
		if(tipo == 3):  # capitao utiliza arma com grande dano e protecao minima, maxima vida
			vida =90 
			numarma = random.randint(10,11)	
			numdefesa = 12
			pescadornovo = Pescador(nome,vida,self.genItensparaIlha(numarma),self.genItensparaIlha(numdefesa))
			return pescadornovo
		else:
			vida 		= 40
			numarma 	= 4
			numdefesa 	= 12
			pescadornovo = Pescador(nome,vida,self.genItensparaIlha(numarma),self.genItensparaIlha(numdefesa))
			return pescadornovo
		

			#sorteia um pescador e adiciona pescador
		# Retorna Se existe id duplicado
	def achaMesmoId(self, identification):	
		tam = len(self.jogadoresAtivos) # verifica tamanho do vetor de jogadores ativos
		if(tam == 0):
			return 0
		else: # procura no vetor
			i =0
			for i in range(tam):
				if(self.jogadoresAtivos[i].getId() ==  identification):
					return 1
			return 0
			

			# retorna o indice da referencia no vetor de personagens Ativos
	def achaReferencia(self, identification):
		tam = len(self.jogadoresAtivos) # verifica tamanho do vetor de jogadores ativos
		if(tam == 0):
			return None
		else: # procura no vetor
			i = 0
			for i in range(tam):
				if(self.jogadoresAtivos[i].getId() ==  identification):
					return i
			return None
		
	
	# devolve somente um indice na tabela de jogadoresAtivos
	def addJogador(self,nome,tipo): # utiliza dois metodos para a insercao, ok
		ref = self.typePescador(nome, tipo)
		vetor = self.addPescador(ref)
		if(vetor!=None):
			novoplayer = Jogador(vetor[0],vetor[1],ref) # novo player retorna um objeto
			# buscando se a identidade esta repetida -------------------------------
			novoplayerid = novoplayer.getId() # pega a identidade do jogador
			retorno = self.achaMesmoId(novoplayerid)
			while (retorno == 1):
				novoplayerid = novoplayer.getId()
				retorno = self.achaMesmoId(novoplayerid)
			#-----------------------------------------------------------------------
			referencia = len(self.jogadoresAtivos) # retorna a refencia no vetor
			self.jogadoresAtivos.append(novoplayer) # adiciona um novo jogador ao jogo
			vetorretorno=[referencia,novoplayerid]
			return vetorretorno # retorna [ilha, IDunico]
		else:
			return None
			# retorna a ilha em que ele foi adicionado e a posicao no vetor de personagens
	def addPescador(self,pescador): #ok
		tam = len(self.arquipelago)
		sorteio =  random.randint(0,tam-1)
	#	print("Ilha:\n",sorteio)
		if(pescador!=None):
			numb = self.arquipelago[sorteio].getNumeroPersonagens() # devolve o numero do jogador
			self.arquipelago[sorteio].addPersonagem(pescador) # adiciona o jogador na ilha especificada 
			vetor = [sorteio,numb]
			return vetor # ilha, jogador
		else:
			return None


			# retorna numero da ilha e o numero do jogador
	
			
			# numbIlha, refere-se a indexacao no vetor arquipelago
			# numbjogador, refere-se ao numero do jogador dentro da ilha
			# numbitem , refere-se a o numero do item que o jogador deseja utilizar

	def jogadorUtilizarItem(self,identificador,numbitem): #ok
		indplayer = self.achaReferencia(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao()
		numbIlha = vetor[0]
		numbjogador = vetor[1]
		a  = self.arquipelago[numbIlha].personagensAtivos[numbjogador].useItem(numbitem)
		if(a == None): 
			message = "Problema Ao utilizar o item" 
			resposta = [vetor[1],message]
			return resposta
		else:
			message = "O item foi utilizado"
			resposta = [vetor[1],message]
			return resposta

	def listarItensDeUmPescador(self, identificador): #ok
		indplayer = self.achaReferencia(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao()
		numbIlha = vetor[0]
		numbjogador = vetor[1]
		message = self.arquipelago[numbIlha].listItensPescador(numbjogador)
		indice = vetor[1]
		saida = [indice,message]
		return (saida)


	def listarItensDeUmaIlha(self,identificador): # ok
		indplayer = self.achaReferencia(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		posicao = self.jogadoresAtivos[indplayer].getPosicao()
		if(self.arquipelago[posicao[0]].retornalistaitensnaIlha()	== None):
			message = "Não há itens no Bau de itens, para a coleta"
			resposta = [posicao[1],message]
			return resposta
		else:
			message = self.arquipelago[posicao[0]].retornalistaitensnaIlha()
			resposta = [posicao[1],message]
			return resposta
		
		# retira o item de uma sala e transfere para o jogador
	def getItemPescador(self,identificador):
		indplayer = self.achaReferencia(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao() 
		numbIlha = vetor[0]
		resposta = self.arquipelago[numbIlha].getItemParaPescador(vetor[1])
		if(resposta == None):
			message = "Erro Ao Transferir o item para o Pescador"
			vectorresponse = [None,message]
			return vectorresponse
		else:
			message = "Sucesso na tranferencia"
			vectorresponse = [None,message]
			return vectorresponse


			# retorna uma string caso exista uma magia na concha da ilha, e outra caso contrario
	def listaMagiaIlha(self,identificador):
		indplayer = self.achaReferencia(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao()
		resposta = self.arquipelago[vetor[0]].verificaSeHaMagia() # caso seja 1 há magia na ilha, caso seja 0 não há magia
		if(resposta ==1):
			message = "Há magia disponivel para a coleta" 
			respostafinal =[vetor[0],message]
			return respostafinal
		else:
			message = "Não há magia disponivel para a coleta" 
			respostafinal = [vetor[0],message]
			return respostafinal
	
		# verifica s há magia na sala
	def checkMagiaIlha(self,numIlha):
		resposta = self.arquipelago[numIlha].verificaSeHaMagia()
		return resposta
	
		# contabiliza todas as magias que se encontram no pode de pescadores
	def contabilizaMagiaColetada(self,num):
		self.magiascoletadas += num
		return None
	
		# retorna o numero de magias em poder de pescadores
	def getMagiasColetadasNoJogo(self):
		return self.magiascoletadas
	
		# transfere uma magia da ilha para um pescador
	def getMagiafromIlha(self,identificador):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao()
		numIlha = vetor[0]
		if(self.checkMagiaIlha(numIlha)==1):
			valormagia = self.arquipelago[numIlha].conchaMagica.getValor() # pega o valor da magia contida na concha
			self.contabilizaMagiaColetada(valormagia)
			value = self.arquipelago[vetor[0]].getMagia(vetor[1])
			if(value == 1):
				message = "Magia Coletada da Ilha"
				vectorresponse = [vetor[0],message]
				return vectorresponse
			else:
				#return "Erro na tranferencia da Magia"
				message = "Erro na tranferencia da Magia"
				vectorresponse = [None,message]
				return vectorresponse

		else:
			message = "Erro na tranferencia da Magia"
			vectorresponse = [None,message]
			return vectorresponse
	
		# retorna o valor das magias do pescador
	def getValorMagiasPescador(self,identificador):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao()
		value = self.arquipelago[vetor[0]].getValorMagias(vetor[1])
		if (value == None):
			message = "Não há valor de magia, Erro critico" 
			resposta =[vetor[0],message]
			return resposta
		else:
			message = "Não há magias, valor: "+str(value) + "\n"
			resposta =[vetor[0],message]
			return resposta
	
		

		# ele atualiza indices da posicao do jogador no vetor de personagens ativos e 
		# atualiza no vetor de jogadores ativos o indice correto
		# Em caso de achar corretamente o indice devolve o numero referente ao indice no vetor de jogadores
		# Em caso de erro retorna None --:> Morreu o player
	def acertaIndices(self, identification):
		indplayer = self.achaReferencia(identification)
		if(indplayer == None): # jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message]
			return vector
		else: # acha no vetor de jogadores onde esta o jogador precisamente(Indice)
			vetorreceptor = self.jogadoresAtivos[indplayer].getPosicao()
			retornodoindice = self.arquipelago[vetorreceptor[0]].refperdida(self.jogadoresAtivos[indplayer].getPescador())
			if(retornodoindice == None): # objeto não se encontra na ilha o jogador morreu			
				return None
			else:
				self.jogadoresAtivos[indplayer].setnovaposicao(vetorreceptor[0],retornodoindice)
				return indplayer # retorna o indice corrigido na posicao do jogadoresAtivos

		#retorna o status do pescador , o argumento é o id do jogador
	def getStatusFromPescadorDeIlha(self,identification):
		indplayer = self.acertaIndices(identification) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		else:
			indice = indplayer # novo valor do indice
			vetor = self.jogadoresAtivos[indice].getPosicao()
			if(vetor[0] > len(self.arquipelago)): # Posicao informada não existe
				message = "Ilha informada não existe"
				vector = [indice,message]
				return vector
			else:
				message = self.arquipelago[vetor[0]].getStatusFromPescador(vetor[1])
				vector  = [indice,message]
				return vector

		# retorna 1 caso somente exista 1 jogador vivo
	def verificaSeSomenteUmEstaVivo(self):
		tamvetor = len(self.jogadoresAtivos) # verifica o tamanho do vetor
		if(tamvetor == 1):	# verifica se somente existe um objeto Jogador
			return 1
		else:
			return 0

	def verificaSeSomenteUmEstaVivoNaIlha(self,identificador):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None or len(self.jogadoresAtivos) == 0): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1] # ultima flag é quando o jogador morreu
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao() # pega a posicao ilha e numero de jogador
		resposta = [indplayer,self.arquipelago[vetor[0]].verificaSeSomenteUmEstaVivo()]
		return (resposta)

	# Acha um jogador no vetor de jogadores, através da busca pelo numero da ilha e do numero do jogador
	def findVetorJogadores(self,numIlha,referenciaSegundoJogador):
		tam = len(self.jogadoresAtivos) # verifica tamanho do vetor de jogador
		i=0
		for i in range(tam):	# percorre objeto em busca de objeto
			obj = self.jogadoresAtivos[i].getPosicao()
			if(obj[0]==numIlha and obj[1]==referenciaSegundoJogador):
				return i
		return None
	
	# Realiza a batalha entre dois personagens dentro das ilhas até a morte de um deles	
	def batalhaPersonagensFromIlha(self,identificador,referenciavetorjogador2):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem	
		#----------------------------------------------------
		vetor1 = self.jogadoresAtivos[indplayer].getPosicao()
		numb   = self.findVetorJogadores(vetor1[0],referenciavetorjogador2)
		if(vetor1[1]==referenciavetorjogador2):
			message =  "Não é possivel mesma referencia de jogador"
			respostafuncao = [None,message]
			return  respostafuncao
		
		if(self.arquipelago[vetor1[0]].getNumeroPersonagens() < referenciavetorjogador2): # 
			message ="Não é possivel a batalha, porque não estão nas mesmas ilhas"
			respostafuncao = [None,message]
			return  respostafuncao	
		
		else:
			retorno = self.arquipelago[vetor1[0]].batalhaPersonagens(vetor1[1],referenciavetorjogador2) # retorna que morreu e a mensagem
			if(retorno[0] == 1): # jogador1 morreu
				if(numb != None):
				#	self.jogadoresAtivos[numb].setnovaposicao(vetor1[0],retorno[1]) # apontando nova posicao			
					ponteirobackup = self.jogadoresAtivos[numb] # aponta para objeto
					del self.jogadoresAtivos[indplayer]
				#	novareferencia2 = jogadoresAtivos.index(numb)
					message = " \n Batalha sucedida Jogador 1 Morreu \n " 
					vetor = [1,message]
					#-----
					numerocorreto = self.jogadoresAtivos.index(ponteirobackup) # retornar numero atual da posicao no vetor de jogadores ativos
					npescador1 = self.jogadoresAtivos[numerocorreto].getPescador() # pega o ponteiro referencia ao objeto
					nindice = self.arquipelago[vetor1[0]].personagensAtivos.index(npescador1) # localiza jogador 2 no vetor
					self.jogadoresAtivos[numerocorreto].setnovaposicao(vetor1[0],nindice) # seta nova posicao de objeto encontrado
					#-----
					return vetor # retorna [quem morreu,nova referencia para jogador 2]
				else:
					del self.jogadoresAtivos[indplayer]
					message = "\nBatalha sucedida Jogador 1 Morreu\n" 
					vetor = [1,message]
					return vetor
			if(retorno[0] == 2): # jogador 2 morreu
				if(numb != None):
				#	self.jogadoresAtivos[jogador1].setnovaposicao(vetor1[0],retorno[1])
					del self.jogadoresAtivos[numb]
				#	jogador1 = self.jogadoresAtivos.index(vetor1)
					message = "\nBatalha sucedida Jogador 2 Morreu\n" 
					vetor = [2,message]
					#-----
					npescador1 = self.jogadoresAtivos[self.acertaIndices(identificador)].getPescador()
					nindice = self.arquipelago[vetor1[0]].personagensAtivos.index(npescador1)
					self.jogadoresAtivos[self.acertaIndices(identificador)].setnovaposicao(vetor1[0],nindice)
					#-----
					return vetor
				else:
				#	self.jogadoresAtivos[jogador1].setnovaposicao(vetor1[0],retorno[1])
					message = "\nBatalha sucedida Jogador 2 Morreu\n" 
					vetor = [2,message]
					#-----
					npescador1 = self.jogadoresAtivos[self.acertaIndices(identificador)].getPescador()
					nindice = self.arquipelago[vetor1[0]].personagensAtivos.index(npescador1)
					self.jogadoresAtivos[self.acertaIndices(identificador)].setnovaposicao(vetor1[0],nindice)
					#------
					return vetor

			if(retorno[0] == 3):
				if(numb != None):
					pont = self.jogadoresAtivos[numb]
					del self.jogadoresAtivos[indplayer]
					novaref = self.jogadoresAtivos.index(pont)
					del self.jogadoresAtivos[novaref] 
					message = "Ambos os jogadores Morreram"
					vetordesaida = [3,message]
					return vetordesaida
				else: # não é um jogador é somente um pirata
					del self.jogadoresAtivos[indplayer]
					message = "Ambos os jogadores Morreram"
					vetordesaida = [3,message]
					return vetordesaida




		# entrada do numero da ilha que se deseja saber os personagens ativos
	def getListdePersonagensfromIlha(self,identificador):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#----------------------------------------------------
	#	print("Indplayer = ",indplayer)
		vetor = self.jogadoresAtivos[indplayer].getPosicao() 
		numb = vetor[0]
		message  = ""
		message += "##########################################################\nIlha de Numero: " + str(numb)+"\n" + self.arquipelago[numb].printpersonagens()
		resposta = [vetor[1],message]
		return resposta
		# troca de ilha numdailha1 ,  o personagem numdopescador1 , a ilha numdailha2  , o personagem numdopescador2 , numdailha1(da onde vem) --> numdailha2(pra onde vai)
	
		# retorna a lista com o nome de todas as direções que a ilha possui, caminhos possiveis para se mover
	def genListadirecoesdaIlha(self,identificador):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#-----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao()
		numilha = vetor[0]
		try:
			respostacompleta =(self.arquipelago[numilha].getListaIlhas()) # consulta na ilha direcoes possiveis			
			vectorspecial = [vetor[0],respostacompleta]
			return vectorspecial
		except IndexError:
			respostacompleta = "Indice alem do limite"
			vectorspecial = [None,respostacompleta]
			return(vectorspecial)

	#def changePersonagemDeIlha (self, numdailha1, numdopescador1, argnumdailha2):
		# jogador é uma posicao no indice de jogadores ativos
	def changePersonagemDeIlha(self, identificador, argnumdailha2):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#----------------------------------------------------
		vetor = self.jogadoresAtivos[indplayer].getPosicao() # retorna vetor com numero da ilha e posicao no vetor do jogador
		numdailha1 = vetor[0]
		numdopescador1 = vetor[1]
		tamarquipelago = self.getNumerodeIlhas() 	# verifica tamanho do vetor de ilhas
		if(type(argnumdailha2) is str): # caso seja string converte em inteiro
			backup = argnumdailha2
			argnumdailha2 = int(backup)

		numdailha2 = argnumdailha2
		if(numdailha1 > tamarquipelago and numdailha2 > tamarquipelago): # verifica se os indices sao menor que o tamanho
			message ="Não há esta ligacao"
			vectorresposta = [vetor[0],message] 
			return vectorresposta
		
		if(self.arquipelago[numdailha1].getNumeroPersonagens() < numdopescador1): # verifica se existem os numero de pescador
			message ="Não há este pescador"
			vectorresposta = [vetor[0],message] 
			return vectorresposta

		else:
			#-----------------------------
			enderecoilhaarray = self.arquipelago.index(self.arquipelago[numdailha1].getIlha(numdailha2)) # retorna objeto da memoria para a comparacao
			#-----------------------------
			numilha2 = self.arquipelago[enderecoilhaarray].getNumeroPersonagens() # verifica quantidade de personagens na ilha de destino
			self.arquipelago[enderecoilhaarray].addPersonagem(self.arquipelago[numdailha1].getPersonagem(numdopescador1))
			self.jogadoresAtivos[indplayer].setnovaposicao(enderecoilhaarray,numilha2)
			message = "Operacao Bem sucedida" # novo endereço do jogador
			vectorresposta = [vetor[0],message] 
			return vectorresposta


	# Realiza a retirada do jogador do Mapa
	def retiraJogador(self,identificador):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # verifica se o jogador morreu
			indice = None
			message = "Você morreu"
			vector = [indice,message,1]
			return vector	# retorna indice no array de jogadores e mensagem
		#----------------------------------------------------
		else:
			vetor = self.jogadoresAtivos[indplayer].getPosicao() # retorna vetor com numero da ilha e posicao no vetor do jogador, vetor[1] -> contem a posicao do jogador
			numdopersonagem = vetor[1]
			self.arquipelago[vetor[0]].retiraPersonagem(vetor[1]) # retira dos personagens o jogador
			del self.jogadoresAtivos[indplayer] # retira o jogador do vetor de jogadores

	# Verifica se Jogador está vivo 
	def verificavida(self,identificador):
		indplayer = self.acertaIndices(identificador) # acha a referencia do jogado 
		#----------------------------------------------------
		if(indplayer == None): # Caso o jogador esteja morto retorna 0
			return 0	
		else:  
			
			if(type(indplayer) is list): # Testando se é um array 
				if(indplayer[0]==None):	#Está morto	
					return 0 
			else: # esta morto
				return 1