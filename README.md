# Trabalho_REDES_2018

Este repositório é referente a um RPG, com temática de piratas. Onde há o uso do sockets TCP para comunicação entre cliente-servidor

# Colaborador

Lucas Rego Da Rocha: <lucasregodarocha@outlook.com>

# License & copyright

© Lucas Rego Da Rocha, Jogo RPG com temática de Pescadores e Piratas.

Licensed under the [MIT License](LICENSE).
