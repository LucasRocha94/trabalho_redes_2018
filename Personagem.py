#!/usr/bin/python3
# -*- coding: utf-8 -*-
#ok
import random
import sys


# from Personagem import * # <- usar a referencia

class Personagem(object):  #ok
	def __init__(self,nome,vida,ataque,defesa):
		self.nome = nome
		self.vida = vida
		self.ataque = ataque
		self.defesa = defesa

	def getNome(self):
		return self.nome

	def getVida(self):
		return self.vida

	def setVida(self,vida):
		self.vida+=int(vida)
		if(self.vida > 100):
			self.vida = 100
		
	def getAtaque(self):
		return self.ataque

	def setAtaque(self,ataque):
		self.ataque = ataque

	def getDefesa(self):
		return self.defesa

	def setDefesa(self,defesa):
		self.defesa = defesa
		# Repassa dano o personagem vivo no primeiro momento após a batalha
	def repassandoDano(self, dano):
		if(dano > self.defesa):
			if(self.defesa == 0):
				self.vida -= dano
				return None
			if(self.defesa != 0):
				backup = self.defesa # salva valor de defesa para o calculo
				self.defesa = 0
				tirardavida = dano - backup # retira da vida do personagem
				self.vida -= tirardavida
				return None
		if(dano == self.defesa): # zera capacidade de defesa
			self.defesa = 0
			return None
		else: # retira da capacidade de defesa o dano 
			self.defesa -= dano
			return None

			# devolve status de personagem
	def getStatus(self):
		resposta =""
		resposta += "\n#########################################################\n"+"Nome do personagem:" + self.getNome() + "\nVida do personagem:" + str(self.getVida())+"\nAtaque do personagem:"+str(self.getAtaque())+"\nDefesa do personagem:"+str(self.getDefesa())+"\n#########################################################\n"
		return resposta		

