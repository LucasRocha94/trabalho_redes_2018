# -*- coding: utf-8 -*-
#!/usr/bin/python3
from socket import *
from codificacaoDecodificacaoCliente import *
import threading
import queue
import time
import random

# Sempre ouve o servidor enviando as respostas do servidor
# Repassando as mensagens para o consumidor e entrando em zona atomica 
class ServerThread(threading.Thread):
	def __init__(self,sockethand,cheio,vazio,atomic,name,opcao,ip,locksaida):
		threading.Thread.__init__(self)
		self.sockethand = sockethand
		#-------------------
		self.cheio= cheio
		self.vazio = vazio
		self.atomic = atomic
		self.identificacao = 0
		#-------------------
		self.nome = name
		self.opcao = opcao
		self.ip = ip
		self.locksaida = locksaida
		#-------------------
	def run(self):
		# manda para um novo objeto um novo endereço de ip
		mensagem = codificaBEGG(self.nome,int(self.opcao),self.ip) # pede um inicio de conexao
		primeiramensagem = bytes(mensagem,"latin-1")
		#---------------------------------------------------
		sockclient.send(primeiramensagem) # envia a primeira mensagem informando que deseja se conectar
		#----------------------------------------------------
		global envio
		global saida # somente quando o socket envia a mensagem close, ele fica em 1, sinal para fechamento de ambas as threads
		saida = 0
		juntando = []
		cont = 0
		m=0
		k=0
		while m==0:
		# Recebe data enviada pelo cliente
			locksaida.acquire()
			if(saida==0): # Testa se foi enviado a mensagem de close
				locksaida.release()
				if (k==0):
					data = self.sockethand.recv(1024) # recebe 1024 bytes de informacao
					mensagemdecode = data.decode("latin-1") # aqui ele recebe a mensagem
					protocolo = decodificaResposta(mensagemdecode)
				else:
					break
			#	print(mensagemdecode)
				#-----------------------------------------------------------------------------
				if(protocolo!=None):
					if(protocolo[0]==0): # tres parametros
						vetorresposta = protocolo[2] # mensagem a o usuário 
						self.identificacao = protocolo[1] # pega a id do usuário
						self.vazio.acquire()
						self.atomic.acquire()
						del envio [:] # esvazia
						envio.append(1) # tipo de variavel enviada
						envio.append(self.identificacao)	
						envio.append(protocolo[2])
						envio.append(0) # adiciona 0 que significa que não realizara a impressao do menu ainda aguardando inicio ...
						self.atomic.release()
						self.cheio.release()
					if(protocolo[0]==1):	
						vetorresposta = protocolo[2] # mensagem a o usuário 
						self.identificacao = protocolo[1] # pega a id do usuário
						self.vazio.acquire()
						self.atomic.acquire()
						del envio [:] # esvazia
						envio.append(1) # tipo de variavel enviada
						envio.append(self.identificacao)	
						envio.append(protocolo[2])
						envio.append(1) # imprime o menu pois jogo funcionando
						self.atomic.release()
						self.cheio.release()
					if(protocolo[0]==7):
						vetorresposta = protocolo[2] # mensagem a o usuário 
						self.identificacao = protocolo[1] # pega a id do usuário
						self.vazio.acquire()
						self.atomic.acquire()
						del envio [:] # esvazia
						envio.append(1) # tipo de variavel enviada
						envio.append(self.identificacao)	
						envio.append(protocolo[2])
						envio.append(1) # imprime o menu pois jogo funcionando
						self.atomic.release()
						self.cheio.release()
					if(protocolo[0]==2): # dois parametros
						self.vazio.acquire()
						self.atomic.acquire()
						if(protocolo[1]==1): # eh segmentado
							if(protocolo[2] > 0):
								numero_faltante = protocolo[2] # faltam diversos pacotes e eles devem ser recebidos
								vetorresposta = protocolo[4]
								# caso seja verificado que se trata de uma 2222 ele não adiciona nada ao consumidor
								for i in range(numero_faltante):
								#	print("Recebendo_unindo")
									info = self.sockethand.recv(1024) # recebe 1024 bytes de informacao
									mensagemdecode = info.decode("latin-1") # aqui ele recebe a mensagem
									protocolo1 = decodificaResposta(mensagemdecode)
									vetorresposta+=protocolo1[4] # soma a mensagem ateh o fim
								#vetorresposta contem a resposta	
								del envio [:] # esvazia
								envio.append(2) # tipo de variavel enviada
								envio.append(vetorresposta)
								envio.append(0) # significa que o jogador ainda pode efetuar jogadas
								self.atomic.release()
								self.cheio.release()
						else:
							vetorresposta =	protocolo[4] # soh pega a mensagem		
							del envio [:] # esvazia
							envio.append(2) # tipo de variavel enviada
							envio.append(vetorresposta)
							envio.append(0) # significa que o jogador ainda pode efetuar jogadas
							self.atomic.release()
							self.cheio.release()	
					if(protocolo[0]==5):# caso protocolo[0] ==5
						self.vazio.acquire()
						self.atomic.acquire()
						#self.sockethand.shutdown()
						self.sockethand.close()
						k+=1	#impede leituras futuras de socket fechado
						vetorresposta = protocolo[1] #mensagem para o usuário
						del envio [:] # esvazia
						envio.append(2) # tipo de variavel enviada
						envio.append(vetorresposta)
						#------------------
						envio.append(2) # jogo acabou
						self.atomic.release()
						self.cheio.release()
					if(protocolo[0]==9): # pode mandar exin ou ficar aguardando mensagem
						self.vazio.acquire()
						self.atomic.acquire()
						vetorresposta = protocolo[1] #mensagem para o usuário
						del envio [:] # esvazia
						envio.append(2) # tipo de variavel enviada
						envio.append(vetorresposta)
						#------------------
						envio.append(1)# encerramento do jogo, jogador pode escolher esperar ou quitar
						self.atomic.release()
						self.cheio.release()	
					#-----------------------------------------------------------------------------
			elif(saida==1):
				locksaida.release()
				print("Encerrado")
				break
			elif not data:break 
		#self.conexao.close()    
		
		return None
# Deve imprimir o menuuser em area atomica
# Deve imprimir os relatorios do jogo
# Deve receber o a id hexa para obter envio de mensagens para o servidor
# Tem missão de enviar todas as mensagens através do objeto socket
class Cons(threading.Thread):
	def __init__(self,sockethand,cheio,vazio,atomic,locksaida):
		threading.Thread.__init__(self)
		self.cheio= cheio
		self.vazio = vazio
		self.atomic = atomic
		self.sockethand = sockethand
		self.identificacao = 0
		self.locksaida = locksaida
	def run(self):
		global envio # variavel global capacidade de enviar o resultado de mensagens do servidor para o cliente
		#global identificacao # id hexa , para envio de mensagen
		global saida
		n=0
		while n==0:
		#	print("Ouvindo!!!")
			self.cheio.acquire()
			self.atomic.acquire()
			if(len(envio)>0):
				if(envio[0]==1):
					self.identificacao = envio[1] # recebe o id hexa
					if(envio[3]==1): # ira imprimir menu e receber entrada
						print('here,156\n',envio[2])
						vector = imprimiMenuRetornaopcoes() # vetor resposta composto por 1 ou mais opcoes
						mensagem = identificaOpcaoConstroiamensagem(vector,self.identificacao) # retorna a mensagem completa , pronta para envio
						self.sockethand.send(mensagem) # envia a mensagem
						if(vector[0]==4): # construcao da mensagem de exin
							time.sleep(2) # dorme por 10 segundos e manda o socket.close
							self.locksaida.acquire() # pede para entrar no lock e termina com ambas as threads 
							saida=1
							n+=1
							self.locksaida.release()
							self.atomic.release()	  
							self.vazio.release() # sai da zona atomica
						
						elif(vector[0]!=4):
							self.atomic.release()	  
							self.vazio.release() # sai da zona atomica
					if(envio[3]==0):# Não faz nada apenas aguarda inicio de jogo			
						print(envio[2])
						self.atomic.release()	
						self.vazio.release()

				if(envio[0]==2): # eh uma mensagem que contêm uma mensagem sem id hexa		
					print("here,168\n",envio[1])# imprime a mensagem
					if(envio[2]==0):# jogo ainda não acabou, pode-se inserir novas jogadas
						vector = imprimiMenuRetornaopcoes() # vetor resposta composto por 1 ou mais opcoes
						mensagem = identificaOpcaoConstroiamensagem(vector,self.identificacao) # retorna a mensagem completa , pronta para envio
						self.sockethand.send(mensagem) # envia a mensagem
						self.atomic.release()	  
						self.vazio.release() # sai da zona atomica
					if(envio[2]==1):# Quando player recebe <9999>
						######################################
							## Envia o Exin
						vetorfinal =[]
						vetorfinal.append(4)
						mensagem = identificaOpcaoConstroiamensagem(vetorfinal,self.identificacao)
						self.sockethand.send(mensagem) # envia a mensagem
						#######################################
						mensagem = identificaOpcaoConstroiamensagem(vector,self.identificacao) # retorna a mensagem completa , pronta para envio
					#	self.sockethand.send(mensagem) # envia a mensagem , exin pedindo fim de conexao
						time.sleep(2) # dorme por 10 segundos e manda o socket.close
						self.locksaida.acquire() # pede para entrar no lock e termina com ambas as threads 
						saida=1
						n+=1
						self.locksaida.release()
						self.sockethand.close()    
						self.atomic.release()	  
						self.vazio.release() # sai da zona atomica
							
					if(envio[2]==2):# Jogo terminou printou quem ganhou pede desligamento recebeu o <5555>
					#	vetorfinal =[]
					#	vetorfinal.append(4)
					#	mensagem = identificaOpcaoConstroiamensagem(vetorfinal,self.identificacao)
					#	self.sockethand.send(mensagem) # envia a mensagem
					#	time.sleep(10) # dorme por 10 segundos e manda o socket.close
						#self.sockethand.close()  
						self.locksaida.acquire() # pede para entrar no lock e termina com ambas as threads 
						saida=1
						n=2
						self.locksaida.release()
					#	print("\nJogo encerrando!!!\n")
					#	self.sockethand.close()
						self.atomic.release()	  
						self.vazio.release() # sai da zona atomica
						
			else:

				self.atomic.release()	  
				self.vazio.release() # sai da zona atomica


# Trata entradas no ImprimiMenuRetornaopcoes()
def trataentradas(enter):
	if(enter==10 or enter ==11 or enter ==12): # caso possua mais alguma entrada
		if(enter ==10):
			print("Digite a posicao atual do personagem\n")
			enter1 = input()
			vetorresta = []
			vetorresta.append(enter)
			vetorresta.append(enter1)
			return vetorresta
		if(enter ==11):
			print("Digite a direcoes que voce deseja se mover\n")
			enter1 = input()
			vetorresta = []
			vetorresta.append(enter)
			vetorresta.append(enter1)
			return vetorresta
		if(enter == 12):
			print("Digite o item que voce deseja utilizar do saco de itens\n")
			enter1 = input()
			vetorresta = []
			vetorresta.append(enter)
			vetorresta.append(enter1)
			return vetorresta
	if(enter < 10 and enter > 0):
		vetorresposta=[]
		vetorresposta.append(enter)
		return vetorresposta


# imprime uma serie de strings contendo o menu
# retorna um vetor com dois valores ou com 1
def imprimiMenuRetornaopcoes(): #Você pode receber o valor das magias do Pescador:------------------------------->10
	print("Digite 1 para : Pegar um item da ilha\n"+"Digite 2 para : Coletar uma magia da ilha\n"+"Digite 3 para : Listar todas as direcoes possiveis a partir da ilha\n"+"Digite 4 para : Encerramento da conexao\n"+"Digite 5 para : Um relatorio dos personagens da ilha\n"+"Digite 6 para : Um relatorio atual da situação atual do jogador\n"+"Digite 7 para : Uma lista de todos os itens do jogador\n"+"Digite 8 para :Uma lista completa de todos os itens presentes na ilha\n"+"Digite 9 para : obter a informação se existe alguma magia presente na ilha atual do jogador\n"+"Digite 10 para : Atacar um personagem\n" + "Digite 11 para : Mover-se para uma determinada Ilha\n" + "Digite 12 para : Utilizar um item do saco de itens\n")
	enterstr = input()
	enter = int(enterstr)
	if(enter>0 and enter<13):
		return(trataentradas(enter))
	else:
		condition = 0
		backup = 0
		while(condition==0): # usuario não digitou entre 0 e 12
			print("Digite 1 para : Pegar um item da ilha\n"+"Digite 2 para : Coletar uma magia da ilha\n"+"Digite 3 para : Listar todas as direcoes possiveis a partir da ilha\n"+"Digite 4 para : Encerramento da conexao\n"+"Digite 5 para : Um relatorio dos personagens da ilha\n"+"Digite 6 para : Um relatorio atual da situação atual do jogador\n"+"Digite 7 para : Uma lista de todos os itens do jogador\n"+"Digite 8 para :Uma lista completa de todos os itens presentes na ilha\n"+"Digite 9 para : obter a informação se existe alguma magia presente na ilha atual do jogador\n"+"Digite 10 para : Atacar um personagem\n" + "Digite 11 para : Mover-se para uma determinada Ilha\n" + "Digite 12 para : Utilizar um item do saco de itens\n")
			enterstr = input()
			enter = int(enterstr)
			backup = enter
			if(enter < 13 and enter > 0):
				condition+=1 # sai do laço
		return(trataentradas(backup))		

#Identifica o tipo de instrucao e retorna uma string com a mensagem
def identificaOpcaoConstroiamensagem(vector,identificacao):
	if(len(vector)==1): # mensagem simples tipo 1 argumento
		mensagem = codificaJOGI(vector[0],identificacao)
		codificacao = bytes(mensagem,"latin-1")
		return codificacao
	if(len(vector)==2):
		if(vector[0]==10): # ATQP
			mensagem = codificaATQP(vector[1],identificacao)
			codificacao = bytes(mensagem,"latin-1")
			return codificacao
		if(vector[0]==11): # MOVP
			mensagem = codificaMOVP(identificacao,vector[1])
			codificacao = bytes(mensagem,"latin-1")
			return codificacao
		if(vector[0]==12): # USEI
			mensagem = codificaUSEI(identificacao,vector[1])	
			codificacao =bytes(mensagem,"latin-1")
			return codificacao



		return None
def opcoesPescador():
	string ='\033[48m'+'\033[0m'+'\033[38m'+"Se voce quiser um marujo ser interpretado por um:\n\n"+"\033[31m"+"Marujo: pouca vida(pequena experiencia em combate), arma de pequena capacidade de ataque, grande protecao, caso deseje este personagem digite 1\n\n"+"\033[33m"+"Intendente: algumas justas(experiencia media em combate), arma intermediaria de ataque, protecao media, digite 2\n\n"+ "\033[34m"+"Capitao dos pescadores: muitas justas(experiencia media em combate), arma ataque maximo , protecao minima, digite 3\n"
	return string






if __name__=='__main__':
	############################
		# tres semaforos resposaveis pelo produtor consumidor 
	cheio  = threading.Semaphore()
	vazio  = threading.Semaphore()
	atomic = threading.Semaphore()
	############################
	cheio.acquire()
	############################
	locksaida = threading.Lock()
	############################
	#j = imprimiMenuRetornaopcoes()
	#g = identificaOpcaoConstroiamensagem(j,"0x12")
	servidorIP = '0.0.0.0'
	servidorPort = 9999
	identificacao = 0
	envio = []
	# Criacao do objeto socket------------------------
	#global sockclient
	sockclient = socket(AF_INET, SOCK_STREAM)
	# Estabelece a conexao com o servidor
	sockclient.connect((servidorIP, servidorPort)) # conecta com o servidor
	i=0
	#-------------------------------------------------
#	primeiramensagem = bytes(mensagem,"latin-1")
	#inicia a conexao
	print("Digite um nome entre 5~32 caracteres")
	nome = input()
	print(opcoesPescador())
	opcao = input()
	ip = '0.0.0.0'
	linhaeescuta = ServerThread(sockclient,cheio,vazio,atomic,nome,opcao,ip,locksaida)
	linhaeescuta.start()
	linhaeescrita = Cons(sockclient,cheio,vazio,atomic,locksaida)
	linhaeescrita.start()
	linhaeescuta.join()
	linhaeescrita.join()
	print("O jogo Encerrou, Boa sorte!!!")

